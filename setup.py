from setuptools import setup, find_packages
import os, sys
from pathlib import Path
import glob
from subprocess import call

install_requires = [
    'psycopg2',
    'geoalchemy2',
    'netCDF4',
    'xarray',
    'sqlalchemy',

    'cyclobs_orm>=0.0.dev0',
    'geopandas-postgis>=0.0.dev0',
    'pathUrl>=0.0.dev0',
    'geo-shapely>=0.0.dev0',
    'atcf>=0.0.dev0',
    'sentinelRequest>=0.0.dev0'
]

setup(name='cyclobs-db',
      description='cyclobs database handling',
      url='https://gitlab.ifremer.fr/cyclobs/cyclobs_db',
      author="Olivier Archer",
      author_email="Olivier.Archer@ifremer.fr",
      license='GPL',
      scripts=[f for f in glob.glob('bin/**') if not os.path.isdir(f)],
      packages=find_packages(),
      include_package_data=True,
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      install_requires=install_requires,
      zip_safe=False
      )
