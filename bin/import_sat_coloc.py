#!/usr/bin/env python
from sqlalchemy import create_engine, and_, or_
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql.expression import false
from cyclobs_orm import Acquisition_SimpleTrack, SimpleTrack, Track, Acquisition, Product, Acquisition_Acquisition

import argparse
import logging
import os
import glob
import xarray as xr
import re

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

# Import sat_coloc products in the database
# These products are added in the Acquisition_Acquisition table in the coloc_product_path column
def import_sat_coloc(sess, root_path):
    files = glob.glob(os.path.join(root_path, "sat_coloc*.nc"), recursive=True)
    row_count = 0

    for f in files:
        # Files doesn't have exactly the same name than in the table
        # so we have to find the corresponding colocations by comparing a part of the filenames
        ds = xr.open_dataset(f)
        f = f.replace("______", "µµµµµµ")
        filenames = str(f).split(sep = "__")
        file1 = re.sub(r'^.*?sat_coloc_', '', filenames[0])
        file1 = file1.replace('-cm-', '-cc-')
        file1 = file1[:44] #44 because of the filenames length
        file2 = filenames[1]
        file2 = file2.replace('-cm-', '-cc-')
        file2 = file2[:44]

        coloc_file = root_path + f.rsplit("/", 1)[1]
        coloc_file = coloc_file.replace("µµµµµµ", "______")

        # Update table rows with names of files
        count = sess.query(Acquisition_Acquisition) \
            .filter(and_(Acquisition_Acquisition.id_Acquisition.contains(file1), Acquisition_Acquisition.id_Acquisition1.contains(file2))) \
            .update({Acquisition_Acquisition.coloc_product_path: coloc_file}, synchronize_session = False)
        row_count += count
        count = sess.query(Acquisition_Acquisition) \
            .filter(and_(Acquisition_Acquisition.id_Acquisition.contains(file2), Acquisition_Acquisition.id_Acquisition1.contains(file1))) \
            .update({Acquisition_Acquisition.coloc_product_path: coloc_file}, synchronize_session = False)
        row_count += count

    sess.commit()
    logger.info(f"Updated {row_count} database rows using {len(files)} files.")


if __name__ == "__main__":
    description = """import sat colocation files into database"""
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("--dbd", action="store", type=str, required=True,
                        help='database (postgresql://user:pass@host:5432/base_name)')
    parser.add_argument("-p", "--path", action="store", type=str, required=True,
                        help='Path to coloc files. Directory will be searched recursively.')

    args = parser.parse_args()

    engine = create_engine(args.dbd)
    Session = sessionmaker(bind=engine)
    sess = Session()

    import_sat_coloc(sess, args.path)
