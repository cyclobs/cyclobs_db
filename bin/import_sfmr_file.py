#!/usr/bin/env python

import time
import argparse
import os
import xarray as xr
import datetime
import geopandas as gpd
import pandas as pd
import geopandas_postgis
from cyclobs_db import timing
from shapely.geometry import LineString
from sqlalchemy import create_engine
import logging

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

HRD = "HRD"
NESDIS = "NESDIS"
sfmr_souces_vars = {
    NESDIS: {
        "wind_speed": "wind_speed",
        "lat": "latitude",
        "lon": "longitude",
        "time": "time",
        "quality": "quality"
    },
    HRD: {
        "wind_speed": "SWS",
        "lat": "LAT",
        "lon": "LON",
        "time": "time",
        "quality": "FLAG"
    }
}


def start_end_datetimes(ds, df, sfmr_source, time_col):
    if sfmr_source == NESDIS:
        start_date = datetime.datetime.strptime(ds.attrs["FileStartDate"], "%a %b %d %X %Y %Z")
        stop_date = datetime.datetime.strptime(ds.attrs["FileEndDate"], "%a %b %d %X %Y %Z")
    elif sfmr_source == HRD:
        start_date = df[time_col].min()
        stop_date = df[time_col].max()
    else:
        msg = f"Unknown SFMR source : {sfmr_source}"
        logger.error(msg)
        raise ValueError(msg)

    return start_date, stop_date

@timing
def import_sfmr_track(files, con, sfmr_source, track_table="Track", simple_track_table="SimpleTrack",
                      footprint_size=100):
    logger.info(f"Importing {len(files)} files into database...")

    # empty Track gdf to get pre-formatted gdf
    gdf_tracks = gpd.GeoDataFrame.from_postgis(f'SELECT * FROM "{track_table}" WHERE sid is NULL', con)
    gdf_simple_tracks = gpd.GeoDataFrame.from_postgis(f'SELECT * FROM "{simple_track_table}" WHERE sid is NULL', con)

    lon = sfmr_souces_vars[sfmr_source]["lon"]
    lat = sfmr_souces_vars[sfmr_source]["lat"]
    wind_speed = sfmr_souces_vars[sfmr_source]["wind_speed"]
    tm = sfmr_souces_vars[sfmr_source]["time"]
    quality = sfmr_souces_vars[sfmr_source]["quality"]

    start_file = time.time()
    for f in files:
        file_time = time.time()
        logger.debug(f"Processing {f}...")
        # Open file
        ds = xr.open_dataset(f)

        # SFMR file /home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/sfmr/noaa_nesdis_star/1.2.0/2021/SFMR210829US006_1-level2.nc
        # has sec dimension == 0 which makes ds.to_dataframe fail. So we skip those files.
        if ("sec" in ds.dims and ds.sec.size == 0) or ("time" in ds.dims and ds.time.size == 0):
            logger.warning(f"File {f} has time/sec dimension empty. Skipping")
            continue

        # Extract each SFMR point
        # Convert xarray dataset to geopandas
        df = ds.to_dataframe()
        logger.debug(f"Read file into dataframe in {time.time() - file_time} sec")
        #print(df)
        #print(f)

        if wind_speed not in df.columns:
            continue

        # Removing NaNs
        df = df[~pd.isna(df[wind_speed])]
        df = df[~pd.isna(df[lat])]
        df = df[~pd.isna(df[lon])]

        logger.debug(f"Remove Nan {time.time() - file_time} sec")

        # Keep only valid data
        if quality in df.columns:
            df = df[df[quality] == 0]

        if sfmr_source == HRD:
            df = df[df["DATE"] != 0]
            df["DATE"] = df["DATE"].astype(int).astype(str)
            df["TIME"] = df["TIME"].astype(int).astype(str)
            df = df[df["DATE"].str.len() == 8]
            df = df[(df["LON"] != 0) & (df["LAT"] != 0)]
            if len(df.index) < 5:
                logger.warning(f"Dataframe for file {f} has less than 5 points. Skipping.")
                continue
            df[tm] = df.apply(lambda x: datetime.datetime.combine(
                    datetime.datetime.strptime(x["DATE"], "%Y%m%d").date(),
                    datetime.datetime.strptime(x["TIME"].zfill(6), "%H%M%S").time()
                ), axis=1)

        # Extract needed global attributes
        start_date, stop_date = start_end_datetimes(ds, df, sfmr_source, tm)

        logger.debug(f"Global attributes {time.time() - file_time} sec")

        # Removing duplicates
        df.drop_duplicates(subset=[tm, wind_speed], inplace=True)

        logger.debug(f"Drop duplicates {time.time() - file_time} sec")

        logger.debug("Creating shapely points...")
        # Create geom from longitude latitude column
        gdf = gpd.GeoDataFrame(
            df, geometry=gpd.points_from_xy(df[lon], df[lat]))
        logger.debug("Done creating shapely points.")
        gdf.rename_geometry("geom", inplace=True)
        gdf.reset_index(inplace=True, drop=True)

        logger.debug(f"Create geom {time.time() - file_time} sec")

        # Adapting geopandas columns to what's expected by the database (drop/rename)
        sfmr_cols = set(list(gdf.columns.values))
        cols_drop = sfmr_cols - {tm, wind_speed, "geom"}
        gdf.drop(columns=cols_drop, inplace=True)
        gdf.rename(columns={tm: "date", wind_speed: "vmax"}, inplace=True)

        # "sid" field is temporarily set to the file path. A postgresql trigger will transform it to the correct
        # cyclone sid.
        gdf["sid"] = f
        gdf["file"] = f
        gdf["vmax"] = gdf["vmax"] * 1.94384

        gdf["source"] = "sfmr_" + sfmr_source

        # Compute global footprint, using 1/10 points : this is enough, as they
        # are close from each other
        logger.debug("Creating global track shape...")
        #global_shape = gdf.iloc[0:-1:1].geometry.apply(
        #    lambda shape: shape.buffer_meters(10000).smallest_dlon())
        global_shape = LineString(gdf.geometry.tolist())
        # If performances issues, can reduce 0.05 to 0.5 or even 2
        global_shape = global_shape.simplify(0.05, preserve_topology=False)
        logger.debug("Done creating global track shape.")
        logger.debug(f"Track shape {time.time() - file_time} sec")

        # This will be inserted in Track table
        # Sid field will be automatically replaced by the right cyclone sid, using a postgresql trigger
        gdf_tracks = gdf_tracks.append(gpd.GeoDataFrame(data={
            "sid": [os.path.basename(f)],
            "source": ["sfmr_" + sfmr_source],
            "file": [f],
            "name": ["placeholder"],
            "startdate": [start_date],
            "stopdate": [stop_date],
            "vmax": [gdf["vmax"].max()],
            "geom": [global_shape]
        }, geometry='geom'))

        gdf_simple_tracks = gdf_simple_tracks.append(gdf)
        end_file_time = time.time()
        logger.debug(f"File processed in {end_file_time - file_time} sec")

    end_file = time.time()
    logger.debug(f"Time taken to read files : {end_file - start_file} sec")
    if len(gdf_simple_tracks.index) > 0:
        time_db = time.time()
        # Insert in Track table
        gdf_tracks.set_index(['sid', 'source', "file"], inplace=True)
        pd.set_option('display.max_colwidth', None)
        logger.info("Inserting whole track into database...")
        gdf_tracks.to_postgis(track_table, con, if_exists="append", temp_method="sql")
        logger.debug("Done inserting whole track into database.")
        time_track = time.time()
        logger.debug(f"Time taken to insert track {time_track - time_db} sec")

        # Insert in SimpleTrack table
        # The source SFMR data is 1 point per second.
        # Take 1 point every 300 seconds
        gdf_simple_tracks = gdf_simple_tracks[::300]
        gdf_simple_tracks.set_index(["sid", "source", "date", "file"], inplace=True)
        logger.info("Inserting track points into database...")
        gdf_simple_tracks.to_postgis(simple_track_table, con, if_exists="append", temp_method="sql")
        logger.info("Done inserting track points into database.")
        time_simple = time.time()
        logger.debug(f"Time taken to insert simple track {time_simple - time_db} sec")

        logger.info(f"Done importing {len(files)} into database.")


if __name__ == "__main__":
    description = """
    Import sfmr track
    """
    # Get start time
    start_time = time.time()

    # Parse the given arguments when the script is executed
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("-f", "--files", action="store", type=str, nargs="*",
                        help='Input SFMR file to import into database', required=True)
    parser.add_argument("-s", "--source", action="store", type=str, required=True,
                        help="SFMR source name. NESDIS or HRD.")
    parser.add_argument("--dbd", action="store", default=os.getenv('DATABASE_URL'), type=str,
                        help='database connection URL (postgresql://user:pass@host:5432/base_name)')
    parser.add_argument("--debug", action="store_true", default=False,
                        help='Enable debug logs')

    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)

    # Connect to an engine so it can send the data to the database
    engine = create_engine(args.dbd)
    con = engine.connect()

    import_sfmr_track(args.files, con, args.source)
