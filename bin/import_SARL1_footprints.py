#!/usr/bin/env python
from sqlalchemy import create_engine

import geopandas as gpd
import geopandas_postgis
geopandas_postgis.add_postgis(gpd)
import sys
import os
import datetime
import argparse
import logging
from pathlib import Path
from shapely.wkt import loads
from sentinelRequest import scihubQuery
import sentinelRequest

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.DEBUG)

sentinelRequest.logger.setLevel(logging.DEBUG)

if __name__ == "__main__":
    description = """import SAR L1 footprints from scihub"""
    parser = argparse.ArgumentParser(description = description)
    
    parser.add_argument("--dbd", action="store", type=str, help='database (postgresql://user:pass@host:5432/base_name)')
    parser.add_argument("sids", action="append", type=str, help='Track sid from Track table')
    args = parser.parse_args()
    if sys.gettrace():
        logger.setLevel(logging.DEBUG)

    sarTable = "SARL1Acq"

    logger.info("DBD : {}".format(args.dbd))
    engine=create_engine(args.dbd)
    
    con=engine.connect()
    
    with con.begin():
        sql_Track='''SELECT * FROM "Track" WHERE sid IN ({sids})'''.format(sids=','.join(["'%s'" % sid for sid in args.sids]))
        gdf = gpd.GeoDataFrame.from_postgis(sql_Track, con, geom_col='geom' )
        for i,track in gdf.iterrows():
            logger.debug("sid : %s , startdate : %s" % (track['sid'],track['startdate']))
            safes=scihubQuery(date=[track['startdate'],track['stopdate']],lonlat=track['geom'],filename='S1?_?W_GRD*',user='oarcher', password='nliqt6u3')
            l1_gdf=gpd.GeoDataFrame(safes)
            l1_gdf['footprint'] = l1_gdf['footprint'].apply(loads)
            l1_gdf.set_geometry('footprint')
            for index,l1 in l1_gdf.iterrows():
                print('%s' % l1['filename'])
    
