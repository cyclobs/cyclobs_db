#!/usr/bin/env python
import sys

import sqlalchemy
from sqlalchemy import create_engine, distinct
from sqlalchemy.orm import sessionmaker
from cyclobs_orm import Acquisition, Product, CenterAnalysis, SimpleTrack, Track

import geopandas as gpd
import geopandas_postgis
import argparse
import logging
import os
import glob
import atcf
from shapely.geometry import Point
import xarray as xr

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


def find_acq_id(id_filename, session):
    try:
        res = session.query(distinct(Acquisition.id)).join(Product, Acquisition.products)\
            .filter(Product.filename.like(f"%{id_filename}%")).one()
    except sqlalchemy.orm.exc.NoResultFound:
        logger.error(f"No acquisition found for acquisition {id_filename}")
        return None

    return res[0]


def find_sid(id_filename, session):
    try:
        res = session.query(distinct(Track.sid)).join(SimpleTrack, Track.simpletracks_cyclone)\
            .join(Acquisition, SimpleTrack.acquisitions).join(Product, Acquisition.products)\
            .filter(Product.filename.like(f"%{id_filename}%")).one()
    except sqlalchemy.orm.exc.NoResultFound:
        logger.error(f"No sid found for acquisition {id_filename}")
        return None

    return res[0]


def build_gdf_row(base_path, dat_file, session, exit_on_error=True):
    logger.debug(dat_file)
    df = atcf.read(dat_file, model="fix")

    id_filename = "-".join(os.path.basename(dat_file).split("-")[4:6])
    id_acq = find_acq_id(id_filename, session)
    if id_acq is None:
        if exit_on_error:
            sys.exit(1)
        return None

    logger.debug(f"Filename ID {id_filename}")
    sid = find_sid(id_filename, session)
    if sid is None:
        if exit_on_error:
            sys.exit(1)
        return None
    logger.debug(f"SID {sid}")
    storm_fix_path = glob.glob(os.path.join(os.path.dirname(dat_file), f"*{sid}.dat"))

    if len(storm_fix_path) == 0:
        logger.error(f"No fix file found for {id_filename} and sid: {sid}.")
        if exit_on_error:
            sys.exit(1)
        return None

    control_plot_path = glob.glob(os.path.join(base_path, f"*{id_filename}*.png"))
    if len(control_plot_path) == 0:
        control_plot_path = None
        #logger.warning(f"No plots found for {dat_file}")
    else:
        control_plot_path = control_plot_path[0]
    try:
        analysis_product_path = glob.glob(os.path.join(base_path, "product", f"*{id_filename}*_geogr_polar.nc"))[0]
    except:
        logger.error(f"Could not find {id_filename} . Skipping")
        return None
    #analysis_product_path = analysis_product_path.replace("_cyclone", "").replace("_gd", "")

    # Radii order is important
    radii = ["64", "50", "34"]

    quadrants = ["ne", "nw", "se", "sw"]

    ds = xr.open_dataset(analysis_product_path)

    data_dict = {
        "id_Acquisition": [id_acq],
        "eye_center": [Point(df.iloc[0]["lon"], df.iloc[0]["lat"])],
        "control_plot_path": [control_plot_path],
        "fix_path": [dat_file],
        "storm_fix_path": [storm_fix_path],
        "analysis_product_path": [analysis_product_path],
        "vmax": [ds.vmax.item()],
        "rmw": [ds.rmax.item()],
        "center_quality_flag": [ds.center_quality_flag.item()]
    }

    for q in quadrants:
        i = 2
        for r in radii:
            data_dict[f"rad_{r}_{q}q"] = [ds[f"geo_radii_ms_{q.upper()}"].item(i)]
            data_dict[f"rad_{r}_{q}q_quality"] = [ds[f"geo_percent_valid_radii_{q.upper()}"].item(i)]
            i += 1

    return gpd.GeoDataFrame(data_dict, geometry="eye_center")


if __name__ == "__main__":
    description = """import tc center analysis"""
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("--dbd", action="store", type=str, required=True,
                        help='database (postgresql://user:pass@host:5432/base_name)')
    parser.add_argument("-p", "--data-path", action="store", type=str, required=True,
                        help='Center analysis data path to process')
    parser.add_argument("--no-exit-error", action="store_true", default=False,
                        help="Do not exit when a file fails to be processed.")
    parser.add_argument("--debug", action="store_true", default=False,
                        help='Enable debug logs')

    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)

    path = args.data_path
    fix_path = os.path.join(path, "fix")
    dbd = args.dbd

    logger.info("DBD : {}".format(dbd))
    engine = create_engine(args.dbd)
    Session = sessionmaker(bind=engine)
    sess = Session()

    dat_files = glob.glob(os.path.join(fix_path, "*owi*.dat"))
    gdf = gpd.GeoDataFrame()
    logger.info("Reading files and generating dataframe...")
    for dat in dat_files:
        row = build_gdf_row(path, dat, sess, exit_on_error=not args.no_exit_error)
        if row is not None:
            gdf = gdf.append(row, ignore_index=True)

    logger.info("Finished generating dataframe.")
    logger.debug(gdf)

    logger.info("Cleaning table...")
    sess.execute('TRUNCATE "Center_analysis";')
    sess.commit()

    logger.info("Inserting into database...")
    gdf.set_index('id_Acquisition', inplace=True)
    gdf.to_postgis("Center_analysis", engine.connect(), if_exists='append')

    logger.info("Finished inserting.")
    sess.commit()
