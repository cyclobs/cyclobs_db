#!/bin/bash

#####################
#
# Usage :
# cat sqlfile | initdb.sh 
# 
#####################


echo "database initialisation" >&2

# DROP CASCADE the whole schema, but we have to reconstruct the permission
psql -q "$DATABASE_URL" -c "DROP SCHEMA IF EXISTS cyclobs CASCADE;" >/dev/null 2>&1
psql -q "$DATABASE_URL" -c "CREATE SCHEMA cyclobs;"
# Set permissions...
psql "$DATABASE_URL" -tAc "SELECT 1 FROM pg_roles WHERE rolname='cyclobs_ro'" | grep -q 1
result=$?
if [ $result -eq 0 ]; then
    echo "cyclobs_ro user detected, setting permissions" >&2
    psql -q "$DATABASE_URL" -c "GRANT USAGE ON SCHEMA cyclobs TO cyclobs_ro;"
    psql -q "$DATABASE_URL" -c "ALTER DEFAULT PRIVILEGES IN SCHEMA cyclobs GRANT SELECT ON TABLES TO cyclobs_ro;"
fi

# dump model into psql, removing shema stuff (read sql file from stdin)
egrep -v "CREATE SCHEMA|ALTER SCHEMA|SET search_path" | psql -q "$DATABASE_URL"
