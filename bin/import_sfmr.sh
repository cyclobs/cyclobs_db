#!/bin/bash

########### Usage ############
# To import from every source :
# import_sfmr.sh
#
# To import from a specific source :
# import_sfmr.sh HRD
# import_sfmr.sh NESDIS

set -e

script_dir=$(dirname $(readlink -f $0)) # ie $CONDA_PREFIX/bin

# echo $script_dir

# Activate our conda environment
if [ -z "$CONDA_PREFIX" ] ; then
    conda_dir=$(readlink -f $script_dir/..)
    . $conda_dir/bin/activate $conda_dir
fi

pkg_dir=$(python -c "print(__import__('pkg_resources').get_distribution('cyclobs-db').location)")/cyclobs_db

# Get DATABASE_URL if not defined
if [ -z "$DATABASE_URL" ] ; then
    source $pkg_dir/etc/cyclobs_db.conf
fi
echo "Database URL : $DATABASE_URL"

if [[ -z "$1" ]]; then
  source="all"
else
  source=$1
fi

#sfmr_files=("/home/cerdata/provider/noaa/aircraft/sfmr/1.1/2019/SFMR190711US009-level2.nc"
#  "/home/cerdata/provider/noaa/aircraft/sfmr/1.1/2019/SFMR190820H1-level2.nc"
#  "/home/cerdata/provider/noaa/aircraft/sfmr/1.1/2019/SFMR190826H1-level2.nc"
#  "/home/cerdata/provider/noaa/aircraft/sfmr/1.1/2018/SFMR180910H1-level2.nc"
#  "/home/cerdata/provider/noaa/aircraft/sfmr/1.1/2018/SFMR180928US010_1-level2.nc"
#  "/home/cerdata/provider/noaa/aircraft/sfmr/1.1/2018/SFMR180808US014-level2.nc")

#sfmr_nesdis=/home/cerdata/provider/noaa/aircraft/sfmr/1.1
if [ "$source" = "NESDIS" ] || [ "$source" = "all" ]; then
  #sfmr_nesdis=/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/sfmr/noaa_nesdis_star/1.1
  #sfmr_files=$(find $sfmr_nesdis -name "SFMR*.nc")
  #printf "%s\n" $sfmr_files | xargs -r -P 5 -L 100 import_sfmr_file.py --dbd "$DATABASE_URL" -s NESDIS -f

  sfmr_nesdis=/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/sfmr/noaa_nesdis_star/1.2.0
  sfmr_files=$(find $sfmr_nesdis -name "SFMR*.nc")
  printf "%s\n" $sfmr_files | xargs -r -P 5 -L 100 import_sfmr_file.py --dbd "$DATABASE_URL" --debug -s NESDIS -f
fi



if [ "$source" = "HRD" ] || [ "$source" = "all" ]; then
  sfmr_hrd=/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/sfmr/noaa_aoml_hrd
  sfmr_files=$(find $sfmr_hrd -name "*.nc")
  #printf "%s\n" $sfmr_files | xargs -n 1 basename | sort | uniq -c
  sfmr_uniq=""
  # Removing duplicated files
  for s in $sfmr_files
  do
    bname=$(basename "$s")
    if [[ $sfmr_uniq != *"$bname"* ]]; then
      sfmr_uniq="$sfmr_uniq $s"
    fi
  done
  printf "%s\n" $sfmr_uniq | xargs -r -P 5 -L 100 import_sfmr_file.py --dbd "$DATABASE_URL" -s HRD -f
fi

#printf "%s\n" "${sfmr_files[@]}" | xargs -r -L 100 import_sfmr_file.py --dbd "$DATABASE_URL" -f

# /home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/sfmr/noaa_aoml_hrd/2014/bertha/NOAA_SFMR20140805I1.nc
# /home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/sfmr/noaa_aoml_hrd/2008/ike/NOAA_SFMR0809120646.nc
# /home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/sfmr/noaa_aoml_hrd/1998/Bonnie/19980824I.hsfmr_v2.0.nc