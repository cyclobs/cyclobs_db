#!/usr/bin/env python
import logging
import os
import argparse
from sqlalchemy.sql import functions

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)   

if __name__ == "__main__":
    description = """import SAR footprints from listing. To be used with shell pipe providing listing names"""
    parser = argparse.ArgumentParser(description = description)
    
    parser.add_argument("--dbd", action="store", type=str, required=True, help='database (postgresql://user:pass@host:5432/base_name)')
    parser.add_argument("-i", "--private-listings", action="store", type=str, nargs='*', default=[], help="""Listing paths that should be set to private into the database.
     If a provided listing is not in the database nothing will happen for it.""")
    parser.add_argument("-p", "--public-listings", nargs='*', default=[], type=str, help="""Listing paths that should be set to public into the database.
    If a provided listing is not in the database nothing will happen for it.""")
    parser.add_argument("--all-public", action="store_true", default=False, help="All the listing found in the database will be set to public. This options overrides -i and -p")
    parser.add_argument("--all-private", action="store_true", default=False, help="All the listing found in the database will be set to private. This options overrides -i and -p")

    args = parser.parse_args()
    
    allPublic = args.all_public
    allPrivate = args.all_private
    listingToPublic = args.public_listings
    listingToPrivate = args.private_listings

    os.environ['DATABASE_URL'] = args.dbd
    #This import will trigger creation of Flask instance and use the DATABASE_URL env var to connect to DB
    from cyclobs_httpd.cyclone import Listing, db

    if allPublic:
        count = db.session.query(Listing).update({Listing.show:"public"})
        db.session.commit()
        logger.info("Set {} listings to public".format(count))
        exit()
    elif allPrivate:
        count = db.session.query(Listing).update({Listing.show:"private"})
        db.session.commit()
        logger.info("Set {} listings to private".format(count))
        exit()
    
    for l in listingToPublic:
        logger.info("Setting {} to public".format(l))
        db.session.query(Listing).filter(db.func.CONCAT(
            Listing.prefix, Listing.commit, '/', Listing.config, '/reports/', Listing.listing)==l).update({Listing.show:"public"}, synchronize_session=False)
        db.session.commit()
    
    for l in listingToPrivate:
        logger.info("Setting {} to private".format(l))
        db.session.query(Listing).filter(db.func.CONCAT(
            Listing.prefix, Listing.commit, '/', Listing.config, '/reports/', Listing.listing)==l).update({Listing.show:"private"}, synchronize_session=False)
        db.session.commit()

    
        
        