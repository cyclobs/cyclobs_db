#!/usr/bin/env python

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import geopandas as gpd
import geopandas_postgis
from shapely.geometry import Point, LineString, Polygon, MultiPolygon
from shapely.ops import unary_union
from shapely.wkt import loads
import argparse
import logging
import sys
import os
import pandas as pd
import cyclobs_db

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)
geopandas_postgis.logger.setLevel(logging.ERROR)

# Iterate through points constituting a line and create points so the distance
# between the line points is inferior to parameter distance
def addEquiPoints(line, distance):
    points = []
    coords = list(line.coords)
    prevPoint = Point(coords[0])
    points.append(prevPoint)
    i = 0
    # Iterate through line points
    while i < len(coords) - 1:
        currPoint = Point(coords[i + 1])
        d = prevPoint.distance(currPoint)
        # If the distance between the previous and current point is still too large
        # creating a new point
        if prevPoint.distance(currPoint) > distance:
            prevPoint = LineString([prevPoint, currPoint]).interpolate(distance)
            points.append(prevPoint)
        # If the distance is OK, jumping to the next point.
        else:
            i += 1
            prevPoint = Point(coords[i])
            points.append(prevPoint)
            
    return LineString(points)

            

if __name__ == "__main__":
    
    
    
    description = """
    import basin to database from shapefile or WKT Polygon 
    """
    parser = argparse.ArgumentParser(description = description)
    
    
    parser.add_argument("--table_name", action="store", default="Basin", type=str, help='table prefix, postfixed with point or line')
    parser.add_argument("--replace", action="store_true", help='drop table if exists')
    parser.add_argument("-n", "--name", action="store", help="Basin shortname")
    parser.add_argument("-f", "--fullname", action="store", help="Basin full name")
    parser.add_argument("--source", action="store", help="Basin source")
    parser.add_argument("-s", "--shapefile", action="store", default=None, type=str, help="basin shapefile")
    parser.add_argument("-p", "--polygon", action="store", default=None, type=str, help="basin polygon")
    parser.add_argument("-m", "--merge-shp", action="store_true", help="Merges into a multi polygon if multiple polygons are found in the shapefile layer")    
    
    args = parser.parse_args()
    if sys.gettrace():
        logger.setLevel(logging.DEBUG)
        
    if args.replace:
        if_exists = 'replace'
    else:
        if_exists = 'append'
        
    database_url=os.getenv('DATABASE_URL')
    if database_url is None:
        raise RuntimeError("DATABASE_URL not defined")
        
    logger.debug("Starting... Basin name : {}, Basin fullname : {}".format(args.name, args.fullname))
        
    if not args.shapefile is None:
        logger.debug("Reading basin shapefile...")
        shp=gpd.read_file(args.shapefile)
        if shp.shape[0] > 1:
            if not args.merge_shp:
                raise ValueError("Shapefile has more than one entry. use --merge-shp to force insert")
            else:
                geom = unary_union(shp.geometry.tolist())
        else:
            geom = shp.geometry.iloc[0]
    elif not args.polygon is None:
        geom = loads(args.polygon)   
    
    logger.debug("Adding equidistant points on Polygon")
    if type(geom) is Polygon:
        logger.debug("Geom type : {}".format(type(geom)))
        lines = list(geom.exterior.coords)
        line = LineString(lines)
        newLine = addEquiPoints(line, 0.5)
        basinPolyg = Polygon(newLine)
    elif type(geom) is MultiPolygon:
        logger.debug("Geom type : {}".format(type(geom)))
        basinPolygs = []
        for polyg in geom:
            lines = list(polyg.exterior.coords)
            line = LineString(lines)
            newLine = addEquiPoints(line, 0.5)
            basinPolygs.append(Polygon(newLine))
        basinPolyg = MultiPolygon(basinPolygs)
    else:
        logger.debug("Incorrect geom type {}".format(type(geom)))
        sys.exit(1)
            
    dict = {
        "name": [args.name],
        "fullname": [args.fullname],
        "geom": [basinPolyg.simplify(0.3)],
        "source": [args.source]
        }
    gdf = gpd.GeoDataFrame(dict, geometry="geom")
    gdf.crs = None
    gdf.set_index('name',inplace=True)
    
    logger.debug("Inserting into DB {}".format(database_url))
    engine=create_engine(database_url)
    con=engine.connect()
    with con.begin():
        gdf.to_postgis(args.table_name,engine, if_exists=if_exists)
        logger.info("Successfully inserted {} basin in DB".format(args.fullname))    
    
    
    

