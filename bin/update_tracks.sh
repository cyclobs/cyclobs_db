#!/bin/bash

days=10

# Définir les options longues et leurs équivalents courts
OPTIONS=$(getopt -o "" --long "all-atcf,all-ibtracs,no-atcf,no-ibtracs,days:" -- "$@")

# Gérer les options et arguments
eval set -- "$OPTIONS"

while true; do
    case "$1" in
        --all-atcf ) all_atcf=True ;;
        --all-ibtracs ) all_ibtracs=True ;;
        --no-atcf ) no_atcf=True ;;
        --no-ibtracs ) no_ibtracs=True ;;
        --days ) days="$2"; shift ;;
        -- ) shift; break ;;
        * ) break ;;
    esac
    shift
done

echo "days: $days"

set -e

script_dir=$(dirname $(readlink -f $0)) # ie $CONDA_PREFIX/bin

# echo $script_dir

# Activate our conda environment
if [ -z "$CONDA_PREFIX" ] ; then
    conda_dir=$(readlink -f $script_dir/..)
    . $conda_dir/bin/activate $conda_dir
fi

pkg_dir=$(python -c "print(__import__('pkg_resources').get_distribution('cyclobs-db').location)")/cyclobs_db

# Get DATABASE_URL if not defined
if [ -z "$DATABASE_URL" ] ; then
    source $pkg_dir/etc/cyclobs_db.conf
fi

if [ "$ALL_ATCF" = "True" ] ; then
  all_atcf="True"
fi

if [ -z "$no_atcf" ]; then
  # We will use the repertory containing all the atcf's files obtained by the merge of two Best-Track repertory
  #url_atcf_best_tracks=/home/datawork-cersat-public/cache/project/hurricanes/analysis/merge-tracks-atcf/
  url_atcf_best_tracks=/home/datawork-cersat-public/cache/project/hurricanes/analysis/best-tracks-atcf-merge
  # Import tracks from atcf
  if [ "$all_atcf" != "True" ] ; then
      current_date=$(date +"%Y%m%d")
      startdate=$(date -d "$current_date -$days days" +"%Y%m%d")
      startdate_msg="since $startdate"
      find_opts="-newermt $startdate"
  else
      startdate_msg="(all)"
  fi

  count=$(find $url_atcf_best_tracks/ -name "*.dat" $find_opts | wc -l)
  if [ "$count" -eq "0" ]; then
    echo "No track file found for the selected interval. Exiting without error."
    exit 0
  fi
  atcf_files=$(find $url_atcf_best_tracks/ -name "*.dat" $find_opts | egrep -v 'b..[89].*' | egrep 'b..[0-9]{6}\.dat')
  atcf_count=$(printf "%s\n" $atcf_files | wc -l)
  echo "Updating $atcf_count atcf files $startdate_msg"
  printf "%s\n" $atcf_files | xargs -r -P 10 -l50 import_atcf.py --footprints -d 9999  || { echo 'Failed to import atcf'; exit 1; }
  echo "Finished to update atcf"
fi

    # ============== #
    # Update ibtracs #
    # ============== #
if [ -z "$no_ibtracs" ]; then
  if [ "$all_ibtracs" != "True" ] ; then

    ibtracs_active="/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/ibtracs/IBTrACS.ACTIVE.list.v04r00.points.shp"
    if [ -f "$ibtracs_active" ]; then
        import_ibtracs.py --footprints "$ibtracs_active" "$DATABASE_URL"
    fi
  else
    ibtracs_file="/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/ibtracs/IBTrACS.since1980.list.v04r00.points.shp"
    if [ -f "$ibtracs_file" ]; then
        import_ibtracs.py --footprints "$ibtracs_file" "$DATABASE_URL"
    fi
  fi
fi




