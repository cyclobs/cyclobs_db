#!/bin/bash

# this script is supposed to be run by root (or with sudo)
# TODO Add parameters for installation directory, QT path, pgmodeler version
# TODO Add help

# Usage : For now the script installs pgmodeler version 0.9.2 in /opt/pgmodeler with QT path=/usr/lib/x86_64-linux-gnu/qt5
# It has been tested on ubuntu 18.04 only

apt-get install -y git libxml2-dev libpq-dev libqt5svg5* qt5-image-formats-plugins libqt5printsupport5 qtbase5-dev qtdeclarative5-dev qt5-default libxkbcommon-x11-dev 

echo "Get pgmodeler and cd into the source dir"
cd /tmp

# from git
git clone https://github.com/pgmodeler/pgmodeler.git
cd pgmodeler


wget https://github.com/pgmodeler/pgmodeler/archive/v0.9.2.tar.gz
tar -xzvf v0.9.2.tar.gz
cd pgmodeler-0.9.2

wd=$PWD

mkdir /opt/pgmodeler
chmod a+rx /opt/pgmodeler

QT_ROOT=/usr/lib/x86_64-linux-gnu/qt5
INSTALLATION_ROOT=/opt/pgmodeler
$QT_ROOT/bin/qmake -r CONFIG+=release PREFIX=$INSTALLATION_ROOT BINDIR=$INSTALLATION_ROOT PRIVATEBINDIR=$INSTALLATION_ROOT PRIVATELIBDIR=$INSTALLATION_ROOT/lib pgmodeler.pro

make -j 20 && make install

cd /usr/lib/x86_64-linux-gnu
cp libQt5DBus.so.5 libQt5PrintSupport.so.5 libQt5Widgets.so.5 libQt5Network.so.5 libQt5Gui.so.5 libQt5Core.so.5 libQt5XcbQpa.so.5 libQt5Svg.so.5 libicui18n.so.* libicuuc.so.* libicudata.so.* $INSTALLATION_ROOT/lib

mkdir $INSTALLATION_ROOT/lib/qtplugins
mkdir $INSTALLATION_ROOT/lib/qtplugins/imageformats
mkdir $INSTALLATION_ROOT/lib/qtplugins/printsupport
mkdir $INSTALLATION_ROOT/lib/qtplugins/platforms

cd $QT_ROOT/plugins
cp -r imageformats/libqgif.so imageformats/libqico.so imageformats/libqjpeg.so imageformats/libqsvg.so imageformats/libqtga.so imageformats/libqtiff.so imageformats/libqwbmp.so $INSTALLATION_ROOT/lib/qtplugins/imageformats
cp -r printsupport/libcupsprintersupport.so $INSTALLATION_ROOT/lib/qtplugins/printsupport
cp -r platforms/libqxcb.so platforms/libqoffscreen.so $INSTALLATION_ROOT/lib/qtplugins/platforms

echo -e "[Paths]\nPrefix=.\nPlugins=lib/qtplugins\nLibraries=lib" > $INSTALLATION_ROOT/qt.conf

cd $wd
cp start-pgmodeler.sh pgmodeler.vars $INSTALLATION_ROOT
chmod +x $INSTALLATION_ROOT/start-pgmodeler.sh

#ln -s /opt/pgmodeler/pgmodeler-cli /usr/bin/pgmodeler-cli
# pgmodeler-cli need a wrapper to run
cat << EOF > /usr/bin/pgmodeler-cli
#!/bin/bash
export PGMODELER_CONF_DIR=/opt/pgmodeler/share/pgmodeler/conf/
export QT_QPA_PLATFORM_PLUGIN_PATH=/usr/lib/x86_64-linux-gnu/qt5/plugins/platforms/
/opt/pgmodeler/pgmodeler-cli -platform minimal $@
EOF
chmod +x /usr/bin/pgmodeler-cli

ln -s /opt/pgmodeler/start-pgmodeler.sh /usr/bin/start-pgmodeler.sh
# convenience, to have a working pgmodeler command
ln -s /opt/pgmodeler/start-pgmodeler.sh /usr/bin/pgmodeler

# test
pgmodeler-cli -plateform offscreen
