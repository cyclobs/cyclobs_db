#!/usr/bin/env python
import datetime

import pandas as pd
import atcf
from shapely.geometry import LineString
from sqlalchemy import create_engine, inspect
import geopandas as gpd
from cyclobs_utils import check_db_dataframe_type_compatibility, knots_to_ms
import os
import argparse
import time
import geopandas_postgis
import logging
import sys
import copy
import glob

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


def read_format_fixes(files: list, days_to_update: int):
    """
    Read each file in given files and store the data in a geopandas GeoDataFrame.
    atcf.linearize is issued for each source (fix site). Columns are renamed to
    match database column names, for insertion. Column types are also set.

    Parameters
    ==========
    files: list List of files (path to files) to read
    days_to_update: int Number of days in the past. Used to only select files created/modified after
    now() - days_to_update.

    Returns
    =======
    geopandas.GeoDataFrame
        Contains all the read data, modified by the function.

    """

    csv_gdf_list = []
    # Select the files to be updated
    atcf_files = atcf.selecting_files_recently_updated(files, days_to_update, model="fix")

    for fix in atcf_files:
        logger.debug('%d/%d : %s' % (atcf_files.index(fix) + 1, len(atcf_files), fix))
        if int(fix[-10:-8]) < 80:  # atcf track number >= 80 are test or invest and should not be ingested
            fix_df, fix_df_analysis = atcf.read(fix, model="fix", multi_return=True)

            # Only keep fix that have a center position and a vmax
            fix_df = fix_df.loc[fix_df["center/intensity"].str.contains("C") &
                                fix_df["center/intensity"].str.contains("I")]
            fix_df_analysis = fix_df_analysis.loc[fix_df_analysis["center/intensity"].str.contains("C") &
                                                  fix_df_analysis["center/intensity"].str.contains("I")]

            # An atcf_fix mixes several data source. For compatibility with database, we will process
            # data by source
            source_list = fix_df["fix site"].unique()
            for source in source_list:
                fix_df_source = fix_df.loc[fix_df["fix site"] == source]
                fix_df_analysis_source = fix_df_analysis.loc[fix_df_analysis["fix site"] == source]
                df_lin = atcf.linearize(fix_df_source, model="fix")

                # When a FIX line type is ANAL (analysis), we can extend columns retrieval for that specific type
                # to get the "observation source", which is later added in the "type" DB column
                if len(fix_df_analysis_source.index) > 0:
                    df_lin_analysis = atcf.linearize(fix_df_analysis_source, model="fix",
                                                     lin_cols=atcf.fix_lin_cols + atcf.cols_fix_analysis)

                # Removing row with necessary data missing
                df_lin = df_lin.loc[~pd.isna(df_lin["lat"]) & ~pd.isna(df_lin["lon"]) & ~pd.isna(df_lin["wind speed"]) &
                                    ~pd.isna(df_lin["fix site"]) & (df_lin["fix site"] != "")]

                # logger.debug(df_lin)
                # sid is the atcf name : ie 'wp162019' for file 'bwp162019.dat'
                df_lin['sid'] = os.path.splitext(os.path.basename(fix))[0][1:]
                # df_lin["date"] = df_lin["date"].apply(lambda x: datetime.datetime.strptime(x, "%Y%m%d%H%M"))
                df_lin["file"] = os.path.basename(fix)
                df_lin["source"] = "fix_atcf_" + df_lin["fix site"]
                df_lin = df_lin.rename(
                    columns={"fix format": "fix_format", "fix type": "fix_type",
                             "fix site": "fix_site",
                             "initials": "fix_enterer",
                             "flagged indicator": "flagged_indicator",
                             "height of ob": "height_observation",
                             "posit confidence": "posit_confidence",
                             "wind speed": "vmax",
                             "confidence": "vmax_confidence",
                             "pressure confidence": "pressure_confidence",
                             "pressure derivation": "pressure_derivation",
                             "MRD": "mrd",
                             "eye diameter": "eye_diameter"
                             })

                if len(fix_df_analysis_source.index) > 0:
                    # Modify fix type to add analysis observation source
                    df_lin.loc[df_lin["fix_type"] == "ANAL", "fix_type"] = df_lin_analysis["observation sources"]\
                        .apply(lambda x: "ANAL " + x if x != "" else "ANAL")

                gd = gpd.GeoDataFrame(df_lin)

                # Create then set the Geometry column
                gd['geom'] = gpd.points_from_xy(gd['lon'], gd['lat'])
                gd = gd.set_geometry('geom')
                csv_gdf_list.append(gd)

    # Return the concatenation of all Tracks in one big df
    if len(csv_gdf_list) > 0:
        conc = pd.concat(csv_gdf_list)

        # This is to prepare a dict with columns types.
        #FIXME Could reduce the number of lines
        fix_types_edited = copy.copy(atcf.fix_lin_cols_types)
        fix_types_edited["fix_format"] = fix_types_edited.pop("fix format")
        fix_types_edited["fix_type"] = fix_types_edited.pop("fix type")
        fix_types_edited["fix_site"] = fix_types_edited.pop("fix site")
        fix_types_edited["fix_enterer"] = fix_types_edited.pop("initials")
        fix_types_edited["flagged_indicator"] = fix_types_edited.pop("flagged indicator")
        fix_types_edited["height_observation"] = fix_types_edited.pop("height of ob")
        fix_types_edited["posit_confidence"] = fix_types_edited.pop("posit confidence")
        fix_types_edited["vmax"] = fix_types_edited.pop("wind speed")
        fix_types_edited["vmax_confidence"] = fix_types_edited.pop("confidence")
        fix_types_edited["pressure_confidence"] = fix_types_edited.pop("pressure confidence")
        fix_types_edited["pressure_derivation"] = fix_types_edited.pop("pressure derivation")
        fix_types_edited["mrd"] = fix_types_edited.pop("MRD")
        fix_types_edited["eye_diameter"] = fix_types_edited.pop("eye diameter")

        conc = atcf.convert_types(conc, fix_types_edited)
    else:
        conc = pd.DataFrame()

    return conc


def check_df_schema(inspector, insert_table_name, gdf):
    """
    Query schema from database table and compare column names and types with given gdf

    Parameters
    ==========

    inspector: obtained from sqlalchemy.inspect function
    insert_table_name: str Table name into which the gdf will be inserted
    gdf: geopandas.GeoDataFrame GeoDataframe to check

    Returns
    =======
    bool
        True if dataframe is OK, False otherwise
    """

    # Obtain column names and types from the database table
    columns_info = inspector.get_columns(insert_table_name)
    db_col_names = [col_info['name'] for col_info in columns_info]
    db_col_types = [col_info['type'] for col_info in columns_info]

    # Check if the column names in the GeoDataFrame match the ones in the database table
    if not set(db_col_names).issubset(set(gdf.columns)):
        logger.error("Column names in the GeoDataFrame do not match the ones in the database table.")
        logger.debug(f"set(db_col_names) - set(gdf.columns): {set(db_col_names) - set(gdf.columns)}")
        return False

    # Check if the data types in the GeoDataFrame match the ones in the database table
    for col_name, col_type in zip(gdf.dtypes.index, gdf.dtypes):
        if col_name in db_col_names:
            logger.debug(f"{col_name}, {str(col_type)}, {str(db_col_types[db_col_names.index(col_name)])}")
            if not check_db_dataframe_type_compatibility(str(col_type),
                                                         str(db_col_types[db_col_names.index(col_name)])):
                logger.error(f"Data type of column {col_name} in the GeoDataFrame does "
                             f"not match the one in the database table.")
                return False

    # If the column names and types match, return True
    return True


def import_atcf_fix(gdf: gpd.GeoDataFrame, con, table_name: str, track_table_name: str,
                    if_exists: str, temp_method: str):
    """
    For each sid (for each track), generate the LineString geometry of the track and prepare
    data for insertion. Then data is inserted into database (first into Track table then atcf_fix)

    Parameters
    ==========
    gdf: geopandas.GeoDataFrame Contains the data to insert (track data), formatted to fit database scheme.
    con: Connection to database (return value of engine.connect())
    table_name: str Table name in which are inserted track points
    track_table_name: str Table name in which are inserted tracks
    if_exists: str See geopandas_postgis.to_postgis documentation
    temp_method: str See geopandas_postgis.to_postgis documentation

    """

    # empty Track df
    gdf_Track = gpd.GeoDataFrame.from_postgis('SELECT * FROM "%s" WHERE sid is NULL' % track_table_name, con)
    sid_list = list(gdf.index.unique('sid'))
    for sid in sid_list:
        logger.debug(f"SID: {sid}")
        t = time.time()
        # loop to compute global shape for an sid
        gdf_sid = gdf[gdf.index.get_level_values('sid') == sid]

        # compute global_shape, using 1/10 points : this is enough, as they
        # are close from each other
        # global_shape = gdf_sid.iloc[0:-1:10].geometry.apply(
        #    lambda shape: shape.buffer_meters(footprint_size * 1000).smallest_dlon())
        # global_shape = unary_union(global_shape)
        global_shape = LineString(gdf_sid.geometry.tolist())
        # If performances issues, can reduce 0.05 to 0.5 or even 2
        global_shape = global_shape.simplify(0.05, preserve_topology=False)

        source_list = list(gdf_sid.index.unique('source'))

        for src in source_list:
            gdf_sid_source = gdf_sid[gdf_sid.index.get_level_values("source") == src]
            gdf_Track = gdf_Track.append(gpd.GeoDataFrame(data={
                "sid": [sid],
                "source": [src],
                "file": [gdf_sid_source.iloc[0]["file"]],
                "name": [gdf_sid_source.iloc[0]["cyclone_name"]],
                "startdate": [gdf_sid_source.index.get_level_values('date').min()],
                "stopdate": [gdf_sid_source.index.get_level_values('date').max()],
                "vmax": [gdf_sid_source["vmax"].max()],
                "geom": [global_shape]
            }, geometry='geom'))
        logger.debug("%d/%s : update footprint for %s in %.1fs" % (
            sid_list.index(sid) + 1, len(sid_list), sid, time.time() - t))

    gdf_Track.set_index(["sid", "source", "file"], inplace=True)
    gdf_Track.to_postgis(track_table_name, con, if_exists=if_exists, temp_method=temp_method)

    gdf.to_postgis(table_name, con, if_exists=if_exists, temp_method=temp_method)


def read_import_atcf_fix(dbd: str, table_name: str, table_track: str, days_to_update: int, files: list,
                         if_exists: str, tmp_method: str, api_url: str):
    """
    Reads and modify atcf_fix data and insert it into database.

    Parameters
    ==========
    dbd: str Database string connection URL
    table_name: str Table name in which track points are inserted
    table_track: str Table name in which tracks are inserted
    days_to_update: int Number of days to update (reduce the number of read files based on their UNIX
        timestamp)
    files: list List of file path to read data from
    if_exists: str See geopandas_postgis documentation
    tmp_method: str See geopandas_postgis documentation
    api_url: str Base URL to cyclobs API, used to get cyclone names from SID, because the
        data doesn't contain cyclone names.
    """

    # Connect to an engine so it can send the data to the database
    engine = create_engine(args.dbd)
    insp = inspect(engine)

    con = engine.connect()

    files_expanded = []
    for f in files:
        files_expanded.extend(glob.glob(f))

    files = files_expanded

    api_data = pd.read_csv(api_url + "?include_cols=sid,cyclone_name")

    gdf = read_format_fixes(files, days_to_update)

    def find_cyc_name(sid):
        sid_df = api_data.loc[api_data["sid"] == sid, "cyclone_name"]
        if len(sid_df.index) > 0:
            return sid_df.iloc[0]
        else:
            return ""

    gdf["cyclone_name"] = gdf["sid"].apply(lambda x: find_cyc_name(x))
    gdf = gdf.loc[gdf["cyclone_name"] != ""]

    if len(gdf.index) > 0:
        if check_df_schema(insp, "atcf_fix", gdf):
            gdf = gdf.set_index(["date", "sid", "source"])
            import_atcf_fix(gdf, con, table_name, table_track, if_exists, temp_method=tmp_method)
        else:
            msg = "Dataframe and database columns or types do not match."
            logger.error(msg)
            raise ValueError(msg)
    else:
        msg = "Dataframe to insert is empty"
        logger.error(msg)
        ValueError(msg)


if __name__ == "__main__":
    description = """Import atcf fix into database. 
    Source of these files is https://www.nrlmry.navy.mil/atcf_web/docs/.fixes/<year> 
    and format description is here : https://www.nrlmry.navy.mil/atcf_web/docs/database/new/newfdeck.txt 
    """

    # Get start time
    start_time = time.time()

    # Parse the given arguments when the script is executed
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("--table-name", action="store", default="atcf_fix", type=str,
                        help='table prefix, postfixed with point or line')

    parser.add_argument("--table-track", action="store", default="Track", type=str,
                        help='table name for tracks')
    parser.add_argument("--dbd", action="store", default=os.getenv('DATABASE_URL'), type=str,
                        help='database (postgresql://user:pass@host:5432/base_name)')
    parser.add_argument("-d", "--days-to-update", action="store", type=int, default=10,
                        help="the number of days to be updated in the past")
    parser.add_argument("files", action="store", nargs='*', type=str,
                        help="atcf tracks stored in unique files : '*.dat'")
    parser.add_argument("--if-exists", action="store", type=str, default="append",
                        help="Chose behavior if table already exist in database. "
                             "Available values : append, replace. Replace not tested")
    parser.add_argument("--tmp-method", action="store", type=str, default="sql",
                        help="See to_postgis to learn about possible values.")
    parser.add_argument("--api-url", action="store", type=str, default="https://cyclobs.ifremer.fr/app/api/getData")
    parser.add_argument("--debug", action="store_true", default=False, help="Enable debug logs")

    args = parser.parse_args()

    if args.debug:
        logger.setLevel(logging.DEBUG)

    if sys.gettrace():
        logger.setLevel(logging.DEBUG)
        geopandas_postgis.logger.setLevel(logging.DEBUG)

    read_import_atcf_fix(args.dbd, args.table_name, args.table_track, args.days_to_update, args.files,
                         args.if_exists, args.tmp_method, args.api_url)
