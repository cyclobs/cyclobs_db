#!/usr/bin/env python

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import geopandas as gpd
import geopandas_postgis
from shapely.geometry import Point, LineString
import argparse
import logging
import sys
import os
import pandas as pd
import datetime
import numpy as np
from collections import OrderedDict
import time
import cyclobs_db
import multiprocessing as mp

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.DEBUG)

engine = None


def sid_to_postgis(gdf, table_name, con_url, if_exists, temp_method):
    engine.dispose()
    with engine.connect() as conn:
        # INFO : Errors like "ERREUR:  les EXCEPT types double precision et character
        # varying ne peuvent pas correspondre" can mean one of these two things :
        # - One column is missing inside the dataframe, compared to the database table scheme
        # - index columns (sid and date) are not 'where they should be'. Yes, it turns out
        #   that the insertion fails if index columns are at the end of the array.
        #   I don't really know why because it isn't an issue for atcf insertion..
        #   So the solution is to modify the DB scheme to but primary keys at the beginning
        #   of the table fields.

        gdf.to_postgis(table_name, conn, if_exists=if_exists, temp_method=temp_method)


def interpolation(dtime, data_frame):
    # Treat case without antimeridian crossing
    if np.nanmax(np.abs(data_frame['LON'].diff())) < 180:
        df = data_frame.set_index('date').resample(dtime).interpolate(method='time').fillna(method='pad')
    # Treat case with antimeridian crossing, by changing projection to [ 0 360 ]
    else:
        data_frame['LON'] = data_frame['LON'] % 360
        df = data_frame.set_index('date').resample(dtime).interpolate(method='time').fillna(method='pad')
        df.loc[df['LON'] >= 180, 'LON'] = df['LON'][df['LON'] >= 180] - 360
    final_df = df.reset_index()
    return final_df


def import_ibtracs(df, dbd, table_name='ibtracs', footprints_table_name='Track', footprints=False, footprint_size=300,
                   if_exists='append', temp_method='pandas'):
    """
        INPUT:
          df : ibtracs geopandas
          table_name : sql table for ibtracs
          footprints_table_name : sql table for footprints (nb triggers not included in code, see model)
          footprints : compute footprints (True or False )
        RETURN
          sid list of updated tracs (if footprints resquested)
    """
    global engine

    ret = None

    engine = create_engine(dbd)
    con = engine.connect()

    sampling_rate_in_minutes = cyclobs_db.get_config(con, key='track_sampling_rate')
    dtime_interp = datetime.timedelta(minutes=int(sampling_rate_in_minutes))

    # Preparing a sql command that will alter the following columns of a given table
    sql_alter = '''ALTER TABLE "{footprints_table_name}" 
        ALTER COLUMN "geom" {_set},
        ALTER COLUMN "name" {_set},
        ALTER COLUMN "startdate" {_set},
        ALTER COLUMN "stopdate" {_set}
        ;'''

    # Alter each column of the table Track to make them nullables
    sql_null = sql_alter.format(_set='DROP NOT NULL', footprints_table_name=footprints_table_name)

    # Alter each column of the table Track to make them not nullables
    sql_notnull = sql_alter.format(_set='SET NOT NULL', footprints_table_name=footprints_table_name)

    if footprints:
        # allow NULL on  footprints_table_name
        con.execute(sql_null)
        pass
    else:
        # no footprints. Make sure to disable existings triggers
        pass

    sid_col = "sid"
    if footprints:
        if if_exists == 'replace':
            con.execute(
                '''DELETE FROM "{footprints_table_name}" WHERE "{footprints_table_name}".sid
                 NOT IN (SELECT sid FROM "SimpleTrack"*) '''.format(
                    footprints_table_name=footprints_table_name))

        gdf_Track = gpd.GeoDataFrame.from_postgis('SELECT * FROM "%s" WHERE sid is NULL' % footprints_table_name, con)
        gdf_Track["source"] = []
        sid_list = list(df[sid_col].unique())
        full_gdf = gpd.GeoDataFrame()
        for sid in sid_list:
            t = time.time()
            # loop to compute global shape for an sid
            df_sid = df.loc[df[sid_col] == sid]

            if len(df_sid.index) > 5:
                df_sid = interpolation(dtime_interp, df_sid)
                gdf_sid = gpd.GeoDataFrame(df_sid, geometry=gpd.points_from_xy(df_sid.LON, df_sid.LAT))
                full_gdf = full_gdf.append(gdf_sid, ignore_index=True)
                # gdf_sid = gdf_sid.set_index(["id", "date"])
                # compute global_shape, using 1/10 points : this is enough, as they
                # are close from each other
                # global_shape = df_sid.iloc[0:-1:10].geometry.apply(
                #    lambda shape: shape.buffer_meters(footprint_size * 1000).smallest_dlon())
                # global_shape = unary_union(global_shape)

                global_shape = LineString(gdf_sid.geometry.tolist())
                # If performances issues, can reduce 0.05 to 0.5 or even 2
                global_shape = global_shape.simplify(0.05, preserve_topology=False)

                gdf_Track = gdf_Track.append(gpd.GeoDataFrame(data={
                    "sid": [sid],
                    "source": ["ibtracs"],
                    "file": ["ibtracs"],  # Forcing file to ibtracs because previously it was
                    # gdf_sid["file"].unique(),and it was causing issues because
                    # actually IBTrACS source can't have multiple file for the same
                    # source (as SFMR can have because it is possible that more than 1 plane
                    # measure the same cyclone, leading to 2 files that can overlap
                    # for the same cyclone).
                    # So settings "file": "ibtracs" instead of the real file name
                    # is a way of saying that for ibtracs source there is one file for all
                    # tracks.
                    "name": [gdf_sid.iloc[0]["NAME"]],
                    "startdate": [gdf_sid["date"].min()],
                    "stopdate": [gdf_sid["date"].max()],
                    "vmax": [gdf_sid["VMAX"].max()],
                    "geom": [global_shape]
                }, geometry='geom'))
                logger.debug("%d/%s : update footprint for %s in %.1fs" % (
                    sid_list.index(sid) + 1, len(sid_list), sid, time.time() - t))
            else:
                df = df.drop(df[df[sid_col] == sid].index)

        # update postgis table
        gdf_Track.set_index(['sid', 'source', 'file'], inplace=True)
        gdf_Track.to_postgis(footprints_table_name, con, if_exists=if_exists, temp_method=temp_method)

        # La
        # Track_to_update = [sid[0] for sid in con.execute(sql_Track_to_update).fetchall()]
        # ret = Track_to_update
        # footprints requested
        # search for null on footprints_table_name, and recompute selected
        # con.execute(sql_update_Track)

        # disallow NULL on footprints_table_name
        con.execute(sql_notnull)

    con.close()

    # FIXME this condition is nonsense. I put it just because full_gdf is only defined if footprints is true
    if footprints:
        full_gdf.set_index(["date", "sid"], inplace=True)
        full_gdf = full_gdf.rename(columns={'geometry': 'geom'}).set_geometry('geom')
        # full_gdf = full_gdf.drop(columns=["file"])

        sid_list = list(full_gdf.index.unique(sid_col))
        gdf_list = []
        for sid in sid_list:
            gdf_list.append((full_gdf[full_gdf.index.get_level_values('sid') == sid].copy(deep=True), table_name, dbd,
                             if_exists, temp_method))

        with mp.Pool(10) as pool:
            pool.starmap(sid_to_postgis, gdf_list)

    return ret


if __name__ == "__main__":
    description = """
    import ibtracs.
    
    to investigate:
      if a track cross the dateline, a message is issued from psql:
      "NOTICE:  Coordinate values were coerced into range [-180 -90, 180 90] for GEOGRAPHY"
      ie for LINESTRING (179.6 -19.9, -178.4 -20.1)
    """
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("--table_name", action="store", default="ibtracs", type=str,
                        help='table prefix, postfixed with point or line')
    parser.add_argument("--footprints", action="store_true", default=True, help='compute footprints. '
                                                                                'NOT TESTED TO FALSE, '
                                                                                'WILL NOT WORK')
    parser.add_argument("--footprints_table_name", action="store", default="Track", type=str,
                        help='table name for footprints')
    parser.add_argument("--replace", action="store_true", default=False, help='drop table if exists')
    parser.add_argument("--index", action="store", default=None, help='select index, ie --index=0:20')
    parser.add_argument("--date_index", action="store", default="20120101:",
                        help='select index by date, ie --date_index=20100101:')
    parser.add_argument("shapefile", action="store", type=str, help="ibtracs shapefile")
    parser.add_argument("dbd", action="store", type=str, help='database (postgresql://user:pass@host:5432/base_name)')

    args = parser.parse_args()
    if sys.gettrace():
        logger.setLevel(logging.DEBUG)
        geopandas_postgis.logger.setLevel(logging.DEBUG)

    gdf = gpd.read_file(args.shapefile)

    if args.index:
        istart, istop = args.index.split(':')
        gdf = gdf[int(istart):int(istop)]

    # schema for types cast ( got from `schema=gpd.io.file.infer_schema(df)`
    logger.debug('current schema : %s' % str(gpd.io.file.infer_schema(gdf)))
    schema = {'geometry': 'Point',
              'properties': OrderedDict(
                  [('SID', 'str'),
                   ('SEASON', 'int'),
                   ('NUMBER', 'int'),
                   ('BASIN', 'str'),
                   ('SUBBASIN', 'str'),
                   ('NAME', 'str'),
                   ('ISO_TIME', 'str'),
                   ('NATURE', 'str'),
                   ('LAT', 'float'),
                   ('LON', 'float'),
                   ('WMO_WIND', 'float'),
                   ('WMO_PRES', 'float'),
                   ('WMO_AGENCY', 'str'),
                   ('TRACK_TYPE', 'str'),
                   ('DIST2LAND', 'int'),
                   ('LANDFALL', 'float'),
                   ('IFLAG', 'str'),
                   ('USA_AGENCY', 'str'),
                   ('USA_ATCFID', 'str'),
                   ('USA_LAT', 'float'),
                   ('USA_LON', 'float'),
                   ('USA_RECORD', 'str'),
                   ('USA_STATUS', 'str'),
                   ('USA_WIND', 'float'),
                   ('USA_PRES', 'float'),
                   ('USA_SSHS', 'float'),
                   ('USA_R34_NE', 'float'),
                   ('USA_R34_SE', 'float'),
                   ('USA_R34_SW', 'float'),
                   ('USA_R34_NW', 'float'),
                   ('USA_R50_NE', 'float'),
                   ('USA_R50_SE', 'float'),
                   ('USA_R50_SW', 'float'),
                   ('USA_R50_NW', 'float'),
                   ('USA_R64_NE', 'float'),
                   ('USA_R64_SE', 'float'),
                   ('USA_R64_SW', 'float'),
                   ('USA_R64_NW', 'float'),
                   ('USA_POCI', 'float'),
                   ('USA_ROCI', 'float'),
                   ('USA_RMW', 'float'),
                   ('USA_EYE', 'float'),
                   ('TOK_LAT', 'float'),
                   ('TOK_LON', 'float'),
                   ('TOK_GRADE', 'float'),
                   ('TOK_WIND', 'float'),
                   ('TOK_PRES', 'float'),
                   ('TOK_R50_DR', 'float'),
                   ('TOK_R50_L', 'float'),
                   ('TOK_R50_S', 'float'),
                   ('TOK_R30_DR', 'float'),
                   ('TOK_R30_L', 'float'),
                   ('TOK_R30_S', 'float'),
                   ('TOK_LAND', 'float'),
                   ('CMA_LAT', 'float'),
                   ('CMA_LON', 'float'),
                   ('CMA_CAT', 'float'),
                   ('CMA_WIND', 'float'),
                   ('CMA_PRES', 'float'),
                   ('HKO_LAT', 'float'),
                   ('HKO_LON', 'float'),
                   ('HKO_CAT', 'str'),
                   ('HKO_WIND', 'float'),
                   ('HKO_PRES', 'float'),
                   ('NEW_LAT', 'float'),
                   ('NEW_LON', 'float'),
                   ('NEW_GRADE', 'str'),
                   ('NEW_WIND', 'float'),
                   ('NEW_PRES', 'float'),
                   ('NEW_CI', 'float'),
                   ('NEW_DP', 'float'),
                   ('NEW_POCI', 'float'),
                   ('REU_LAT', 'float'),
                   ('REU_LON', 'float'),
                   ('REU_TYPE', 'float'),
                   ('REU_WIND', 'float'),
                   ('REU_PRES', 'float'),
                   ('REU_TNUM', 'float'),
                   ('REU_CI', 'float'),
                   ('REU_RMW', 'float'),
                   ('REU_R34_NE', 'float'),
                   ('REU_R34_SE', 'float'),
                   ('REU_R34_SW', 'float'),
                   ('REU_R34_NW', 'float'),
                   ('REU_R50_NE', 'float'),
                   ('REU_R50_SE', 'float'),
                   ('REU_R50_SW', 'float'),
                   ('REU_R50_NW', 'float'),
                   ('REU_R64_NE', 'str'),
                   ('REU_R64_SE', 'str'),
                   ('REU_R64_SW', 'str'),
                   ('REU_R64_NW', 'str'),
                   ('BOM_LAT', 'float'),
                   ('BOM_LON', 'float'),
                   ('BOM_TYPE', 'float'),
                   ('BOM_WIND', 'float'),
                   ('BOM_PRES', 'float'),
                   ('BOM_TNUM', 'float'),
                   ('BOM_CI', 'float'),
                   ('BOM_RMW', 'float'),
                   ('BOM_R34_NE', 'float'),
                   ('BOM_R34_SE', 'float'),
                   ('BOM_R34_SW', 'float'),
                   ('BOM_R34_NW', 'float'),
                   ('BOM_R50_NE', 'float'),
                   ('BOM_R50_SE', 'float'),
                   ('BOM_R50_SW', 'float'),
                   ('BOM_R50_NW', 'float'),
                   ('BOM_R64_NE', 'float'),
                   ('BOM_R64_SE', 'float'),
                   ('BOM_R64_SW', 'float'),
                   ('BOM_R64_NW', 'float'),
                   ('BOM_ROCI', 'float'),
                   ('BOM_POCI', 'float'),
                   ('BOM_EYE', 'float'),
                   ('BOM_POS_FL', 'float'),
                   ('BOM_PRS_FL', 'float'),
                   ('NAD_LAT', 'float'),
                   ('NAD_LON', 'float'),
                   ('NAD_CAT', 'float'),
                   ('NAD_WIND', 'float'),
                   ('NAD_PRES', 'float'),
                   ('WEL_LAT', 'float'),
                   ('WEL_LON', 'float'),
                   ('WEL_WIND', 'float'),
                   ('WEL_PRES', 'float'),
                   ('DS8_LAT', 'float'),
                   ('DS8_LON', 'float'),
                   ('DS8_STAGE', 'str'),
                   ('DS8_WIND', 'float'),
                   ('DS8_PRES', 'float'),
                   ('TD6_LAT', 'float'),
                   ('TD6_LON', 'float'),
                   ('TD6_STAGE', 'float'),
                   ('TD6_WIND', 'float'),
                   ('TD6_PRES', 'str'),
                   ('TD5_LAT', 'float'),
                   ('TD5_LON', 'float'),
                   ('TD5_WIND', 'float'),
                   ('TD5_PRES', 'float'),
                   ('TD5_ROCI', 'float'),
                   ('NEU_LAT', 'float'),
                   ('NEU_LON', 'float'),
                   ('NEU_CLASS', 'str'),
                   ('NEU_WIND', 'float'),
                   ('NEU_PRES', 'float'),
                   ('MLC_LAT', 'float'),
                   ('MLC_LON', 'float'),
                   ('MLC_CLASS', 'str'),
                   ('MLC_WIND', 'float'),
                   ('MLC_PRES', 'float'),
                   ('USA_GUST', 'float'),
                   ('BOM_GUST', 'float'),
                   ('BOM_GUSTP', 'float'),
                   ('REU_GUST', 'str'),
                   ('REU_GUSTP', 'float'),
                   ('USA_SEAHGT', 'float'),
                   ('USA_SEA_NE', 'float'),
                   ('USA_SEA_SE', 'float'),
                   ('USA_SEA_SW', 'float'),
                   ('USA_SEA_NW', 'float'),
                   ('STORM_SPD', 'float'),
                   ('STORM_DR', 'float'),
                   ('year', 'int'),
                   ('month', 'int'),
                   ('day', 'int'),
                   ('hour', 'int'),
                   ('min', 'int')]
              )
              }

    # Delete the unused columns by the GeoDataFrame based on the given schema
    dtypes = dict(schema['properties'])
    for col in list(dtypes):
        if col not in gdf:
            logger.debug("in dtypes, but not in df: %s" % col)
            del dtypes[col]
    st = gdf["STORM_DR"]
    gdf = gdf.astype(dtypes, copy=False)

    # Write the length of the GDF to tell the number of rows of the readed File
    logger.debug("Read %d records from %s" % (len(gdf), args.shapefile))

    # Convention naming
    gdf.rename(columns={"SID": "sid", "ISO_TIME": "date"}, inplace=True)

    # Drop rows that contains no storm id
    gdf = gdf.loc[gdf['USA_ATCFID'] != 'None']

    # Forcing file to ibtracs because previously it was
    # gdf_sid["file"].unique(),and it was causing issues because
    # actually IBTrACS source can't have multiple file for the same
    # source (as SFMR can have because it is possible that more than 1 plane
    # measure the same cyclone, leading to 2 files that can overlap
    # for the same cyclone).
    # So settings "file": "ibtracs" instead of the real file name
    # is a way of saying that for ibtracs source there is one file for all
    # tracks.
    gdf["source"] = "ibtracs"

    # Change "sid" column content with "USA_ATCF_ID", and use another format for the date
    gdf['sid'] = gdf['USA_ATCFID'].str.lower()
    gdf["sid"] = gdf["sid"].astype(str)

    gdf['date'] = pd.to_datetime(gdf['date'], format="%Y-%m-%d %H:%M:%S")

    gdf["file"] = "ibtracs"

    # date_index selection
    if args.date_index:
        startdate, stopdate = args.date_index.split(":")
        startdate = datetime.datetime.strptime(startdate, "%Y%m%d")
        if not stopdate:
            stopdate = "22001231"
        stopdate = datetime.datetime.strptime(stopdate, "%Y%m%d")
        logger.info(f"Keeping points between {startdate} and {stopdate}")
        logger.info(f"Length before {len(gdf.index)}")
        gdf = gdf.loc[(gdf['date'] >= np.datetime64(startdate)) & (
                gdf['date'] <= np.datetime64(stopdate))]
        logger.info(f"Length after {len(gdf.index)}")

    gdf = gdf.drop_duplicates(subset=['sid', 'date'])

    # Define index (will be pk in db)
    # gdf.set_index(['id', 'date'], inplace=True)

    # VMAX
    gdf['VMAX'] = gdf['USA_WIND']
    # 1st fallback : VMAX - replace the row where VMAX is NULL by the maximum sustained wind speed from the WMO agency 
    nans = np.isnan(gdf['VMAX'])
    gdf.loc[nans, 'VMAX'] = gdf['WMO_WIND'][nans]
    # 2st fallback : all mean
    vmax_keys = [w for w in gdf.keys() if '_WIND' in w]
    nans = np.isnan(gdf['VMAX'])
    gdf.loc[nans, 'VMAX'] = gdf[vmax_keys].max(axis=1)[nans]

    # Converting GeoDataFrame to DataFrame because later interpolation doesn't work with geometries
    df = pd.DataFrame(gdf.drop(columns="geometry"))

    # We haven't any Coordinate Reference Systems => Geographic type
    # df.crs = None

    # Geometries are already in the df because they are read from the shapefiles
    # df = df.rename(columns={'geometry': 'geom'}).set_geometry('geom')

    if args.replace:
        if_exists = 'replace'
    else:
        if_exists = 'append'

    # clean existing triggers
    sids = import_ibtracs(df, args.dbd, table_name=args.table_name, footprints_table_name=args.footprints_table_name,
                          footprints=args.footprints, if_exists=if_exists)
    # df.to_postgis(args.table_name,con,if_exists=if_exists,temp_method='sql')

    if sids:
        logger.info("updated %s Tracks" % (len(sids)))
