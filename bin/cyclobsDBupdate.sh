#!/bin/bash

# Notes :
# FOR PRODUCTION, DATABASE_URL must be defined before using this script

GETOPT=$(getopt -n $0 -o : --long all-atcf,no-atcf -- $@)
eval set -- "$GETOPT"

while true; do
    case "$1" in
        --all-atcf ) all_atcf=True ; shift ;;
        --no-atcf ) no_atcf=True ; shift ;;
        -- ) shift ; break ;;
        * ) echo "unknown opt $1" >&2 ; exit 1 ;;
    esac
done

set -e

# ============== #
# Initialisation #
# ============== #

# Verifying if it's already done
if [ "$CYCLOBS_DB_FILL" == "False" ] ; then
    echo "skippind database fill (CYCLOBS_DB_FILL == $CYCLOBS_DB_FILL)"
    exit 0    
fi

script_dir=$(dirname $(readlink -f $0)) # ie $CONDA_PREFIX/bin

# echo $script_dir

# Activate our conda environment
if [ -z "$CONDA_PREFIX" ] ; then
    conda_dir=$(readlink -f $script_dir/..)
    . $conda_dir/bin/activate $conda_dir
fi

pkg_dir=$(python -c "print(__import__('pkg_resources').get_distribution('cyclobs-db').location)")/cyclobs_db

# Get DATABASE_URL if not defined
if [ -z "$DATABASE_URL" ] ; then
    source $pkg_dir/etc/cyclobs_db.conf
fi
echo "Database URL : $DATABASE_URL"

# ========================= #
# Updating SAR acquisitions #
# ========================= #

echo "Updating SAR acquisitions..."
# get processing directory from https://cerweb.ifremer.fr/shoc/shoc_dailyupdate_names.html
proc_dir=$(dirname $(curl -s https://cerweb.ifremer.fr/shoc/shoc_dailyupdate_names.html | grep 'http-equiv="refresh"' | sed -r 's/.*URL=(.*)".*/\1/' | xargs pathUrl.py --schema=dmz -p))

import_acquisition.py --dbd "$DATABASE_URL" -p "$proc_dir" --acq_type SAR -t swath || { echo 'Failed to import SAR with swath type'; exit 1; }
import_acquisition.py --dbd "$DATABASE_URL" -p "$proc_dir" --acq_type SAR -t gridded || { echo 'Failed to import SAR with gridded type'; exit 1; }
echo "Finished to update SAR acquisitions."

# ========================== #
# Updating SMOS acquisitions #
# ========================== #
echo "Updating SMOS acquisitions..."
smos_dir=/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/gridded/smos
import_acquisition.py --dbd "$DATABASE_URL" -p /home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/smos/ --acq_type SMOS -t swath || { echo 'Failed to import SMOS with gridded type'; exit 1; }
import_acquisition.py --dbd "$DATABASE_URL" -p $smos_dir --acq_type SMOS -t gridded || { echo 'Failed to import SMOS with gridded type'; exit 1; }
echo "Finished to update SMOS acquisitions."

# ========================== #
# Updating SMAP acquisitions #
# ========================== #
echo "Updating SMAP acquisitions..."
smap_dir=/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/gridded/smap
import_acquisition.py --dbd "$DATABASE_URL" -p $smap_dir --acq_type SMAP -t gridded || { echo 'Failed to import SMAP with gridded type'; exit 1; }
echo "Finished to update SMAP acquisitions."

# ============================== #
# Importing SAT collocalisations #
# ============================== #
echo "Importing SAT collocalisation..."
coloc_files_dir=/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/coloc/sat_sat/product/
import_sat_coloc.py --dbd "$DATABASE_URL" -p $coloc_files_dir || { echo 'Failed to import SAT collocalisations'; exit 1; }
echo "Finished to import SAT collocalisations."


# ======================== #
# Updating ibtracs' tracks #
# ======================== #

# Download ibtracs
# TODO : choice to make between downloading the "lines" file, or the "points" file (if only it matters anyway)
#url_ibtracks_lines=ftp://eclipse.ncdc.noaa.gov/pub/ibtracs/v04r00/provisional/shapefiles/IBTrACS.last3years.list.v04r00.lines.zip
#url_ibtracks_points=ftp://eclipse.ncdc.noaa.gov/pub/ibtracs/v04r00/provisional/shapefiles/IBTrACS.last3years.list.v04r00.points.zip
#echo "Downloading ibtracs last3years from $url_ibtracks_lines"
#echo "Downloading ibtracs last3years from $url_ibtracks_points"
#ibName="ibtracs3years_lines.zip"
#ibName="ibtracs3years_points.zip"
#mkdir -p /tmp/ibtracs
#wget $url_ibtracks_lines -O /tmp/ibtracs/$ibName
#wget $url_ibtracks_points -O /tmp/ibtracs/$ibName
#echo "Download finished."

# Unzipping ibtracs
#cd /tmp/ibtracs
#echo "Unzipping ibtracs..."
#unzip -o $ibName
#echo "Unzipping ibtracs finished."

# Import all the tracks from ibtracs for the last 3 years
#echo "Updating ibtracs..."
#import_ibtracs.py --footprints /tmp/ibtracs/IBTrACS.last3years.list.v04r00.lines.shp "$DATABASE_URL"
#import_ibtracs.py --footprints /tmp/ibtracs/IBTrACS.last3years.list.v04r00.points.shp "$DATABASE_URL"
#echo "Finished to update ibtracs"

# ====================== #
# Updating atcf's tracks #
# ====================== #

if [ "$all_atcf" = "True" ] ; then
  export ALL_ATCF=True
fi

if [ -z "$no_atcf" ]; then
  $script_dir/update_tracks.sh --no-ibtracs
fi

# ============== #
# Update ibtracs #
# ============== #
$script_dir/update_tracks.sh --no-atcf


####################
# Update SFMR data #
####################
#import_sfmr.sh HRD
#import_sfmr.sh NESDIS

######################
# Updating SFMR data #
######################
import_sfmr_coloc.py --dbd "$DATABASE_URL" -p /home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/coloc/sfmr

#############################
# Updating tc analysis data #
#############################
#import_center_analysis.py --dbd "$DATABASE_URL" -p /home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/tc_center/
#import_center_analysis.py --dbd "$DATABASE_URL" -p /home1/scratch/tcevaer/center/
import_center_analysis.py --dbd "$DATABASE_URL" -p /home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/tc_center/



