#!/bin/bash

#####################
#
# Usage :
# sudo bin/setup_postgis_root.sh "postgres://user:passwd@host:port/database"
# 
# can also be invoked without sudo to check that database access is ok
#####################

# Check if there is the parameter "postgres://user:passwd@host:port/database" in the command given in the terminal
if [ -n "$1" ] ; then
    export DATABASE_URL="$1"
fi

if [ -z "$DATABASE_URL" ] ; then
    echo "no DATABASE_URL env var defined" >&2
    echo "ie : export DATABASE_URL=postgres://cyclobs:xxxxxx@flot:5433/cyclobs'"
    exit 1
fi

# check if the base is already uo
echo '\dx' | psql  "$DATABASE_URL" | grep postgis
if [ $? -eq 0 ] ; then
    echo "INFO: "$DATABASE_URL" allready up"
    exit 0
fi

if [ "$EUID" -ne 0 ]; then
    echo "please run sudo $0 '$DATABASE_URL'"
    exit 2
fi
    

# Put the different elements composing the command into variables
export PGUSER=$(echo "$DATABASE_URL" | grep -Po "//\K[^:]*")
passwd=$(echo "$DATABASE_URL" | grep -Po "${PGUSER}:\K[^@]*")
export PGHOST=$(echo "$DATABASE_URL" | grep -Po "${PGUSER}:${passwd}@\K[^:]*")
export PGPORT=$(echo "$DATABASE_URL" | grep -Po "${PGUSER}:${passwd}@${PGHOST}:\K[^/]*")
base=$(echo "$DATABASE_URL" | grep -Po "${PGUSER}:${passwd}@${PGHOST}:${PGPORT}/\K.*")

# Check if the Host given in parameter is the same as the current computer
if [ "$PGHOST" != $(hostname) ] ; then
    echo "ERROR: PGHOST $PGHOST doesn't match current hostname $(hostname)" >&2
    exit 1
fi


# Create a temporary file containing the cluster configuration
cat > /tmp/createcluster.conf <<EOF
# Default values for pg_createcluster(8). Occurrences of '%v' are replaced by the major version number, and '%c' by the cluster name. Use '%%' for a literal '%'.

# Create a "main" cluster when a new postgresql-x.y server package is installed
# create_main_cluster = true

# Default start.conf value, must be one of "auto", "manual", and "disabled". See pg_createcluster(8) for more documentation.
# start_conf = 'auto'

# Default data directory.
# data_directory = '/var/lib/postgresql/%v/%c'

# Default directory for transaction logs. Unset by default, i.e. pg_xlog remains in the data directory.
# xlogdir = '/var/lib/postgresql/xlog/%v/%c/pg_xlog'

# Options to pass to initdb.
# initdb_options = ''

# The following options are copied into the new cluster's postgresql.conf:

# Enable SSL by default (using the "snakeoil" certificates installed by the ssl-cert package, unless configured otherwise here)
ssl = on

# Put stats_temp_directory on tmpfs
stats_temp_directory = '/var/run/postgresql/%v-%c.pg_stat_tmp'

# Add prefix to log lines
log_line_prefix = '%%t [%%p-%%l] %%q%%u@%%d '

listen_addresses = '*'
EOF
# End Of File created


# Install postgis (Spatial and Geographic Objects for PostgreSQL)
apt-get install postgresql-10 postgresql-10-postgis-2.5

# Get PostgreSQL version from the main cluster
pg_ver=$(pg_lsclusters -h | grep main | awk '{print $1}')


# Select the directory where we are going to create a new PostgreSQL server cluster
export PGDATA=/mnt/chunk1/home/pg_cyclobs #export PGDATA=/home/flot/project/cyclobs/pgdata
# Create the directory
mkdir -p $PGDATA
# Give it's propriety to "postgres" user
chown postgres  $PGDATA
# Create the cluster with the configuration written just before
pg_createcluster -c /tmp/createcluster.conf -u postgres -d $PGDATA -p $PGPORT $pg_ver $base

rm /tmp/createcluster.conf

# Configure the Client Authentication using TCP/IP using MD5 to verify the user's password
PG_HBA="host    all             all             0.0.0.0/0               md5"
if ! grep -q "$PG_HBA" /etc/postgresql/$pg_ver/$base/pg_hba.conf ; then
    echo "$PG_HBA"  >> /etc/postgresql/$pg_ver/$base/pg_hba.conf
fi

# Start the PostgreSQL's server
systemctl start postgresql@${pg_ver}-$base

# Delete PGHOST
unset PGHOST

# Write in the terminal (this part seems to be not working)
echo "CREATE USER $PGUSER WITH PASSWORD '$passwd' ;" | su postgres -c "psql -p $PGPORT postgres postgres"
echo "CREATE DATABASE $base WITH OWNER = $PGUSER ; " | su postgres -c "psql -p $PGPORT postgres postgres"
echo "CREATE EXTENSION postgis" | su postgres -c "psql -p $PGPORT -d $base postgres"
echo "ALTER ROLE $PGUSER IN DATABASE $base SET search_path TO pg_catalog,public,cyclobs;" | su postgres -c "psql -p $PGPORT -d $base postgres"

# check everything works as expected
echo '\dx' | psql  "$DATABASE_URL" | grep postgis
if [ $? -ne 0 ] ; then
    echo "CRITICAL: unable to setup database $DATABASE_URL" >&2
    exit 1  
fi
