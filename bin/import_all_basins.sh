#!/bin/bash

# shapefile are located in package directory (see setup.py)

DATA_DIR=$(python -c "print(__import__('pkg_resources').get_distribution('cyclobs-db').location)")/cyclobs_db

# set DATABASE_URL if not defined
source $DATA_DIR/etc/cyclobs_db.conf


# See ftp://eclipse.ncdc.noaa.gov/pub/ibtracs/v04r00/doc/IBTrACS_version4_Technical_Details.pdf for basin definition

import_basin.py --name NI --fullname "North Indian" --source "ibtracs" --polygon "POLYGON ((30 31, 100 31, 100 0, 30 0, 30 31))" 
import_basin.py --name SI --fullname "South Indian" --source "ibtracs" --polygon "POLYGON ((10 0, 135 0, 135 -60, 10 -60, 10 0))" 
import_basin.py --name SP --fullname "South Pacific" --source "ibtracs" --polygon "MULTIPOLYGON (((135 0, 180 0, 180 -60, 135 -60, 135 0)), ((-180 0, -67 0, -67 -60, -180 -60, -180 0)))" 
#import_basin.py --name SA --fullname "South Atlantic" --source "ibtracs" --shapefile SA.shp "$DATABASE_URL" #http://www.marineregions.org/gazetteer.php?p=details&id=1914
import_basin.py --name NWP --fullname "Northwest Pacific" --source "ibtracs" --polygon "POLYGON ((100 51, 180 51, 180 0, 100 0, 100 51))"
#import_basin.py --name NEP --fullname "Northeast Pacific" --source "ibtracs" --shapefile $DATA_DIR/var/shapes/NEP.shp  #http://www.marineregions.org/gazetteer.php?p=details&id=24116
import_basin.py --name NEP --fullname "Northeast Pacific" --source "ibtracs" --polygon "POLYGON ((-180 51, -137 58, -96.6 15.6, -94.96 16.24, -78 6, -78 0, -180 0, -180 51))"
#import_basin.py --name NA --fullname "North Atlantic" --source "ibtracs" --shapefile $DATA_DIR/var/shapes/NA.shp  "$DATABASE_URL" #http://www.marineregions.org/gazetteer.php?p=details&id=23629
#The shapefile ENA.shp has been created by combining NA.shp, CS.shp and GM.shp with the following commands
# ogr2ogr -f "ESRI Shapefile" cyclobs_db/var/cyclobs/shapes/ENA/ENA.shp cyclobs_db/var/cyclobs/shapes/NA/NA.shp
# ogr2ogr -f "ESRI Shapefile" -append -update cyclobs_db/var/cyclobs/shapes/ENA/ENA.shp cyclobs_db/var/cyclobs/shapes/CS/CS.shp
# ogr2ogr -f "ESRI Shapefile" -append -update cyclobs_db/var/cyclobs/shapes/ENA/ENA.shp cyclobs_db/var/cyclobs/shapes/GM/GM.shp
# This is meant to make Carribean Sea and Golf Mexico child basins of North Atlantic.
import_basin.py --name NA --fullname "North Atlantic" --source "ibtracs" --merge-shp --shapefile $DATA_DIR/var/shapes/ENA.shp  #http://www.marineregions.org/gazetteer.php?p=details&id=23629
import_basin.py --name WA --fullname "Western Australia" --source "ibtracs" --polygon "POLYGON ((90 0, 135 0, 135 -60, 90 -60, 90 0))" 
import_basin.py --name EA --fullname "Eastern Australia" --source "ibtracs" --polygon "POLYGON ((135 0, 160 0, 160 -60, 135 -60, 135 0))" 
import_basin.py --name AS --fullname "Arabian Sea" --source "ibtracs" --polygon "POLYGON ((30 31, 78 31, 78 0, 30 0, 30 31))"
import_basin.py --name BB --fullname "Bay of Bengal" --source "ibtracs" --polygon "POLYGON ((78 31, 100 31, 100 0, 78 0, 78 31))" 
#import_basin.py --name CP --fullname "Central Pacific" --source "ibtracs" --polygon "POLYGON ((-180 51, -140 57.6, -140 0, -180 0, -180 51))"
import_basin.py --name CS --fullname "Caribbean Sea" --source "ibtracs" --shapefile $DATA_DIR/var/shapes/CS.shp  #http://www.marineregions.org/gazetteer.php?p=details&id=4287
import_basin.py --name GM --fullname "Gulf of Mexico" --source "ibtracs" --shapefile $DATA_DIR/var/shapes/GM.shp  #http://www.marineregions.org/gazetteer.php?p=details&id=4288

