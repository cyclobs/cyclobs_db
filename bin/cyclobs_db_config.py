# -*- coding: utf-8 -*-
#!/usr/bin/env python

import os, sys, logging, argparse, datetime
import cyclobs_db
import pandas as pd
from sqlalchemy import create_engine


logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


description = " Get global constants from Constants table "
parser = argparse.ArgumentParser(description = description)

parser.add_argument("-k", "--key", action="store", default=None , help='key name')
parser.add_argument("-v", "--value", action="store", default=None, help='set value for key')
parser.add_argument("--dbd", action="store", default=os.getenv('DATABASE_URL'), type=str, help='database (postgresql://user:pass@host:5432/base_name)')

args = parser.parse_args()

if sys.gettrace():
    logger.setLevel(logging.DEBUG)
    

# Connect to an engine so it can get the data from the database, or send it
engine = create_engine(args.dbd)
con = engine.connect()

if args.key is None and args.value is not None:
    raise ValueError("can't set value for None key")

# If at least one value is specified, then we will set or update a value in Constants table 
if args.value is not None :
    # set mode
    cyclobs_db.set_config(con,args.key,args.value)
    
else :
    # get mode
    result = cyclobs_db.get_config(con,key=args.key)
    print(result)
        




