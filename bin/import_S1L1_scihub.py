#!/usr/bin/env python

from sqlalchemy import create_engine
import geopandas as gpd
import geopandas_postgis
import argparse
import logging
import sys
import os
import geo_shapely as geo_shp
import datetime 
import sentinelRequest as sr
sr.default_user = 'oarcher'
sr.default_password = 'xxxx'
sr.default_cachedir='/home1/scratch/oarcher/scihub_cache'
sr.default_filename='S1?_?W_GRD*.SAFE'

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

def fetch_atcf(con,table_name,startdate=None,stopdate=None):
    # read atcf from postgis table
    logger.debug("reading table %s" % (table_name))
    datesel = []
    if startdate is not None:
        datesel.append("date>='%s'" % startdate)
    if stopdate is not None:
        datesel.append("date<'%s'" % stopdate)
    datesel = " AND ".join(datesel)
    if len(datesel) > 0:
        datesel = "where %s" % datesel
    
    sql = 'select sid,date,vmax,geom from "%s" %s' % (args.table_name, datesel )
    gdf_tracks  = gpd.GeoDataFrame.from_postgis(sql, con, geom_col='geom' )
    gdf_tracks.crs = {'init' : 'epsg:4326'}
    gdf_tracks.set_index(['sid','date'], drop=True,inplace=True)
    logger.debug("read  %s tracks points" % len(gdf_tracks))
    
    return gdf_tracks  

def coloc_S1(gdf_tracks,width_km=100,freq=datetime.timedelta(minutes=15)):
    
    if sys.gettrace():
        import matplotlib.pyplot as plt
        fig = plt.figure()
    else:
        fig = None

    
    # coloc area around center
    gdf_tracks.geometry = gdf_tracks.geometry.apply(lambda shape : shape.buffer_meters(1000000))
    #gdf_tracks.geometry = gdf_tracks.geometry.buffer(1)
    
    # time range
    gdf_tracks['beginposition'] = gdf_tracks.index.get_level_values('date') - freq/2
    gdf_tracks['endposition'] = gdf_tracks.index.get_level_values('date') + freq/2
    # sampling should came from a table (see https://gitlab.ifremer.fr/cyclobs/cyclobs_db/issues/3)
    safes = sr.scihubQuery(gdf_tracks,min_sea_percent=1,fig=fig)
    if fig:
        plt.show()
    safes.drop_duplicates(['filename'],keep='first',inplace=True)
    coloc = safes[['filename','footprint']].merge(gdf_tracks[['geom']],left_index=True,right_index=True)  

    return coloc


if __name__ == "__main__":
    description = """
    colocalize points (ie from cyclone track) with scihub to get S1 L1
    """

    # Parse the given argument when the script is executed
    parser = argparse.ArgumentParser(description = description)
    parser.add_argument("--table_name", action="store", default="SimpleTrack", type=str, help='table to read to get atcf tracks')
    parser.add_argument("--startdate",action="store",default=None,help='start date from table, ie "2019-02-03"')
    parser.add_argument("--stopdate",action="store",default=None,help='stop date from table, ie "2019-08-03"')
    parser.add_argument("--dbd", action="store", type=str, default=os.getenv('DATABASE_URL', None) , help='database (postgresql://user:pass@host:5432/base_name)')

    args = parser.parse_args()
    if sys.gettrace():
        logger.setLevel(logging.DEBUG)

    # Connect to an engine to get access to the database
    engine = create_engine(args.dbd)
    con = engine.connect()
     
    with con.begin():
        
        gdf_tracks = fetch_atcf(con, args.table_name, startdate=args.startdate, stopdate=args.stopdate)
        gdf_tracks = gdf_tracks[gdf_tracks.index.get_level_values('sid') == 'al052019']

        
    freq=datetime.timedelta(minutes=15) # FIXME read from table, see https://gitlab.ifremer.fr/cyclobs/cyclobs_db/issues/3)
    safes = coloc_S1(gdf_tracks, width_km=100, freq=freq)
    
    for track_idx,safe in safes.iterrows():
        print('%s %s' % (track_idx , safe.filename))
    pass
        
        