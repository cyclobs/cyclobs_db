#!/usr/bin/env python
from sqlalchemy import create_engine, and_
from sqlalchemy.orm import sessionmaker
from cyclobs_orm import Acquisition_SimpleTrack, SimpleTrack, Track, Acquisition, Product

import argparse
import logging
import os
import glob
import xarray as xr

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


def import_sfmr_coloc(sess, root_path):
    files = glob.glob(os.path.join(root_path, "**", "*.nc"), recursive=True)

    row_count = 0

    for f in files:
        ds = xr.open_dataset(f)
        sat_file = ds.attrs["filename_compared"].replace("_gd", "_ll_gd")
        sfmr_file = ds.attrs["filename_sfmr"]
        count = sess.query(Acquisition_SimpleTrack) \
            .filter(and_(Acquisition.id == Acquisition_SimpleTrack.id_Acquisition,
                         Product.id_Acquisition == Acquisition.id,
                         Product.filename.like(f"%{sat_file}"),
                         SimpleTrack.date == Acquisition_SimpleTrack.date_SimpleTrack,
                         SimpleTrack.sid == Acquisition_SimpleTrack.sid_SimpleTrack,
                         SimpleTrack.source == Acquisition_SimpleTrack.source_SimpleTrack,
                         SimpleTrack.file == Acquisition_SimpleTrack.file_SimpleTrack,
                         Track.sid == SimpleTrack.sid,
                         Track.source == SimpleTrack.source,
                         Track.file == SimpleTrack.file,
                         Track.file.like(f"%{sfmr_file}"))) \
            .update({Acquisition_SimpleTrack.coloc_file: f}, synchronize_session=False)
        row_count += count

    sess.commit()
    logger.info(f"Updated {row_count} database rows using {len(files)} files.")


if __name__ == "__main__":
    description = """import SFMR colocation files into database"""
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("--dbd", action="store", type=str, required=True,
                        help='database (postgresql://user:pass@host:5432/base_name)')
    parser.add_argument("-p", "--path", action="store", type=str, required=True,
                        help='Path to coloc files. Directory will be searched recursively.')

    args = parser.parse_args()

    engine = create_engine(args.dbd)
    Session = sessionmaker(bind=engine)
    sess = Session()

    import_sfmr_coloc(sess, args.path)
