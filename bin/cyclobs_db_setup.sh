#!/bin/bash


GETOPT=$(getopt -n $0 -o : --long initdb,help -- $@)
eval set -- "$GETOPT"

while true; do
    case "$1" in
        --initdb ) initdb=True ; shift ;;
        -- ) shift ; break ;;
        * ) echo "unknown opt $1" >&2 ; exit 1 ;;
    esac
done



# get cyclobs-db install path 
package_location=$(python -c "print(__import__('pkg_resources').get_distribution('cyclobs-db').location)")/cyclobs_db

echo "cyclobs-db found : $package_location" >&2

# get sys.prefix (ie conda root dir)
sys_prefix=$(python -c "import sys; print(sys.prefix)")


if [ -z "$DATABASE_URL" ] && [ ! -f $package_location/etc/cyclobs_db_url ] ; then
    echo "error : DATABASE_URL must be defined" >&2
    exit 1
fi

# set $CONDA_PREFIX/etc/cyclobs_db.conf
#python -c "import pkg_resources,sys ; sys.stdout.buffer.write(pkg_resources.resource_string('cyclobs_db','etc/cyclobs_db.conf'))" > $CONDA_PREFIX/etc/cyclobs_db.conf


. $package_location/etc/cyclobs_db.conf

echo "using DATABASE_URL=$DATABASE_URL"

cyclobs_url_file=$package_location/etc/cyclobs_db_url

echo -n "$DATABASE_URL" > $cyclobs_url_file

# check database is up
setup_postgis_root.sh
if [ $? -eq 2 ] ; then
    echo "database not reachable" >&2
    echo "use sudo setup_postgis_root.sh to create it, and rerun this script" >&2
    exit 1
fi


if [ "$initdb" == "True" ] ; then
    echo "creating database structure from model"
    pgmodeler-cli -v 10.0 -s -ef -if  $package_location/etc/model.dbm -of $package_location/etc/model.sql 2>&1
    if [ ${PIPESTATUS[0]} -ne 0 ] || [ ! -f $package_location/etc/model.sql ] ; then
        echo "error while exporting model to sql file" >&2
        exit 1
    fi
    cat $package_location/etc/model.sql | timeout 30 initdb.sh
    if [ $? -ne 0 ] ; then
        echo "error while initializing db" >&2
        echo "is the database locked ? Active connections are:" >&2
        echo "select pid,client_addr from pg_stat_activity where datname = 'cyclobs';" | psql "$DATABASE_URL"
        echo "you can kill them with:"
        echo "select pg_terminate_backend(pid) from pg_stat_activity where datname='cyclobs' and pid<>pg_backend_pid(); | psql '$DATABASE_URL'"
        exit 1
    fi

fi

# import basins
import_all_basins.sh

cyclobsDBupdate.sh #--all-atcf

