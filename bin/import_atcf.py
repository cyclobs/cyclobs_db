#!/usr/bin/env python

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import geopandas as gpd
import geopandas_postgis
import string
from numpy import NaN
from shapely import wkt
from shapely.geometry import Point, LineString, MultiPolygon
from shapely.ops import unary_union
import geo_shapely  # for buffer_meters
import argparse
import logging
import sys
import os
import time
import pandas as pd
import datetime as dt
import numpy as np
from collections import OrderedDict
import atcf
import cyclobs_db

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)


def import_atcf(gdf, con, table_name='atcf', footprints_table_name='Track', footprints=True, footprint_size=300,
                if_exists='append', temp_method='sql'):
    """
        INPUT:
          df : atcf geopandas
          table_name : sql table for SimpleTrack
          footprints_table_name : sql table for footprints (nb triggers not included in code, see model)
          footprints : compute footprints (True or False )
        RETURN
          sid list of updated tracks (if footprints requested)
    """
    ret = None

    # If footprints==True, update Track
    if footprints:
        # update footprints_table_name with the global shape's track
        if if_exists == 'replace':
            con.execute('''DELETE FROM "{footprints_table_name}" 
                        WHERE "{footprints_table_name}".sid NOT IN (SELECT sid FROM "SimpleTrack"*) 
                        '''.format(footprints_table_name=footprints_table_name))

        # empty Track df
        gdf_Track = gpd.GeoDataFrame.from_postgis('SELECT * FROM "%s" WHERE sid is NULL' % footprints_table_name, con)
        gdf_Track["source"] = []
        sid_list = list(gdf.index.unique('sid'))
        for sid in sid_list:
            t = time.time()
            # loop to compute global shape for an sid
            gdf_sid = gdf[gdf.index.get_level_values('sid') == sid]

            # compute global_shape, using 1/10 points : this is enough, as they
            # are close from each other 
            #global_shape = gdf_sid.iloc[0:-1:10].geometry.apply(
            #    lambda shape: shape.buffer_meters(footprint_size * 1000).smallest_dlon())
            #global_shape = unary_union(global_shape)

            global_shape = LineString(gdf_sid.geometry.tolist())
            # If performances issues, can reduce 0.05 to 0.5 or even 2
            global_shape = global_shape.simplify(0.05, preserve_topology=False)

            gdf_Track = gdf_Track.append(gpd.GeoDataFrame(data={
                "sid": [sid],
                "source": ["atcf"],
                "file": [gdf_sid.iloc[0]["file"]],
                "name": [gdf_sid.iloc[0]["NAME"]],
                "startdate": [gdf_sid.index.get_level_values('date').min()],
                "stopdate": [gdf_sid.index.get_level_values('date').max()],
                "vmax": [gdf_sid["VMAX"].max()],
                "geom": [global_shape]
            }, geometry='geom'))
            logger.debug("%d/%s : update footprint for %s in %.1fs" % (
            sid_list.index(sid) + 1, len(sid_list), sid, time.time() - t))

        # update postgis table
        gdf_Track.set_index(['sid', 'source', 'file'], inplace=True)
        gdf_Track.to_postgis(footprints_table_name, con, if_exists=if_exists, temp_method=temp_method)

    # dump atcf to postgis in table_name
    gdf.to_postgis(table_name, con, if_exists=if_exists, temp_method=temp_method)

    return ret


def interpolation(dtime, data_frame):
    # Treat case without antimeridian crossing
    if np.nanmax(np.abs(data_frame['lon'].diff())) < 180:
        df = data_frame.set_index('date').resample(dtime).interpolate(method='time').fillna(method='pad')
    # Treat case with antimeridian crossing, by changing projection to [ 0 360 ]
    else:
        data_frame['lon'] = data_frame['lon'] % 360
        df = data_frame.set_index('date').resample(dtime).interpolate(method='time').fillna(method='pad')
        df.loc[df['lon'] >= 180, 'lon'] = df['lon'][df['lon'] >= 180] - 360
    final_df = df.reset_index()
    return final_df


def read_dat(dtime, list_of_dat, nb_days_to_update=10):
    csv_gdf_list = []
    # Select the files to be updated
    atcf_files = atcf.selecting_files_recently_updated(list_of_dat, nb_days_to_update)
    # Read each file
    for atcf_file in atcf_files:
        logger.debug('%d/%d : %s' % (atcf_files.index(atcf_file) + 1, len(atcf_files), atcf_file))
        if int(atcf_file[-10:-8]) < 80:
            # atcf track number >= 80 are test or invest and should not be ingested
            try:
                # Read the file
                csv_df = atcf.read(atcf_file)
                # only one date per line
                csv_df = atcf.linearize(csv_df)

            except Exception as e:
                logger.error("skipping unreadable %s" % atcf_file)
                if sys.gettrace():
                    raise e
                continue

            # sid is the atcf name : ie 'wp162019' for file 'bwp162019.dat'
            csv_df['sid'] = os.path.splitext(os.path.basename(atcf_file))[0][1:]
            # csv_df['sid'] = "%s%02d%s" % (csv_df['BASIN'][0].lower() , csv_df['CY'][0] , csv_df['date'][0].strftime("%Y"))

            # atcf format doesn't define 'STORMNAME' for all entry. get it from atcf.getName
            csv_df['STORMNAME'] = atcf.getName(csv_df)
            csv_df.rename(columns={'STORMNAME': 'NAME'}, inplace=True)

            csv_df['VMAX'] = csv_df['VMAX'].map(lambda l: float(l))

            csv_df = interpolation(dtime, csv_df)

            # Cast string column's values into their type, then verify if they aren't null
            apply_on_nan_str(csv_df,
                             ['TECH', 'TY', 'SUBREGION', 'INITIALS', 'DEPTH', 'userdata1', 'userdata2', 'userdata3',
                              'userdata4', 'userdata5', 'USERDEFINED1', 'USERDEFINED2', 'USERDEFINED3', 'USERDEFINED4',
                              'USERDEFINED5'])

            # Convert the DataFrame in a GeoDataFrame
            gd = gpd.GeoDataFrame(csv_df)
            gd["file"] = os.path.basename(atcf_file)
            gd["source"] = "atcf"
            csv_gdf_list.append(gd)
        else:
            logger.debug("Skipping non official %s" % atcf_file)

    # Return the concatenation of all Tracks in one big df
    if len(csv_gdf_list) > 0:
        conc = pd.concat(csv_gdf_list)
    else:
        conc = pd.DataFrame()
    return conc


def read_gpkg(dtime, list_path_filename):
    for path_filename in list_path_filename:
        gdf = gpd.read_file(path_filename)

        # Modify the data of the GeoDataFrame
        gdf.reset_index()
        gdf.drop(columns=['SEAS_12_NEQ', 'SEAS_12_SEQ', 'SEAS_12_SWQ', 'SEAS_12_NWQ', 'USERDEFINED1', 'userdata1',
                          'USERDEFINED2', 'userdata2', 'USERDEFINED3', 'userdata3', 'USERDEFINED4', 'userdata4',
                          'USERDEFINED5', 'userdata5',
                          'year', 'month', 'geometry'], inplace=True)

        # Convert the GeoDataFrame back to a DataFrame
        df = pd.DataFrame(gdf)

        # Cast float column's values into their type, then verify if they aren't None
        apply_on_none_float(df, ['TECHNUM/MIN', 'MSLP', 'RAD_34_NEQ', 'RAD_34_SEQ', 'RAD_34_SWQ', 'RAD_34_NWQ',
                                 'RAD_50_NEQ',
                                 'RAD_50_SEQ', 'RAD_50_SWQ', 'RAD_50_NWQ', 'RAD_64_NEQ', 'RAD_64_SEQ', 'RAD_64_SWQ',
                                 'RAD_64_NWQ', 'ROUTER', 'POUTER',
                                 'RMW', 'GUSTS', 'EYE', 'MAXSEAS', 'DIR', 'SPEED'])

        # Modify some essentials data
        df.rename(columns={'STORMNAME': 'NAME'}, inplace=True)
        df['VMAX'] = gdf['VMAX'].map(lambda l: float(l))
        df["date"] = gdf["date"].apply(lambda x: dt.datetime.strptime(str(x), '%Y%m%d%H'))

        # Interpolate each track
        list_df = []
        for sid in df.sid.unique():
            df_by_sid = df.loc[df['sid'] == sid]
            list_df.append(interpolation(dtime, df_by_sid))

        # Return a GeoDataFrame
        return gpd.GeoDataFrame(pd.concat(list_df))


def verify_schema(gdf):
    # schema is used for type conversion between gpd and postgis
    # logger.debug('current schema : %s' % str(gpd.io.file.infer_schema(df)))
    schema = {'geometry': 'Point',
              'properties': OrderedDict(
                  [('sid', 'str'), ('date', 'str'), ('source', 'str'), ('NAME', 'str'), ('VMAX', 'float'),
                   ('lat', 'float'),
                   ('lon', 'float'), ('BASIN', 'str'), ('CY', 'float'), ('TECHNUM/MIN', 'float'), ('TECH', 'str'),
                   ('TAU', 'float'), ('MSLP', 'float'), ('TY', 'str'), ('RAD', 'float'), ('WINDCODE', 'str'),
                   ('RAD1', 'float'), ('RAD2', 'float'), ('RAD3', 'float'), ('RAD4', 'float'), ('POUTER', 'float'),
                   ('ROUTER', 'float'), ('RMW', 'float'), ('GUSTS', 'float'), ('EYE', 'float'), ('SUBREGION', 'str'),
                   ('MAXSEAS', 'float'), ('INITIALS', 'str'), ('DIR', 'float'), ('SPEED', 'float'), ('DEPTH', 'str'),
                   ('USERDEFINED1', 'str'), ('userdata1', 'str'), ('USERDEFINED2', 'str'), ('userdata2', 'str'),
                   ('USERDEFINED3', 'str'), ('userdata3', 'str'), ('USERDEFINED4', 'str'), ('userdata4', 'str'),
                   ('USERDEFINED5', 'str'), ('userdata5', 'str')]
                  # ('SEAS', 'str'),('SEASCODE', 'str'),('SEAS1', 'float'),('SEAS2', 'float'),('SEAS3', 'float'),('SEAS4', 'float'),
              )}

    # Make sure schema is valid (ie every entry from schema match a column in df)
    dtypes = dict(schema['properties'])
    for col in list(dtypes):
        if col not in gdf:
            logger.debug("in dtypes, but not in df: {}".format(col))
            del dtypes[col]


def apply_on_nan_str(my_gdf, list_strcol_gdf):
    for strcol_gdf in list_strcol_gdf:
        my_gdf[strcol_gdf] = my_gdf[strcol_gdf].astype(str)
        my_gdf.loc[my_gdf[strcol_gdf] == 'nan', strcol_gdf] = ''


def apply_on_none_float(my_gdf, list_strcol_gdf):
    for strcol_gdf in list_strcol_gdf:
        my_gdf[strcol_gdf] = my_gdf[strcol_gdf].astype(float)
        my_gdf.loc[my_gdf[strcol_gdf] is None, strcol_gdf] = NaN


if __name__ == "__main__":
    description = """
    import atcf.
    to investigate:
      if a track cross the dateline, a message is issued from psql:
      "NOTICE:  Coordinate values were coerced into range [-180 -90, 180 90] for GEOGRAPHY"
      ie for Point (179.6 -19.9)
      ie for LINESTRING (179.6 -19.9, -178.4 -20.1)
    """
    # Get start time
    start_time = time.time()

    # Parse the given arguments when the script is executed
    parser = argparse.ArgumentParser(description=description)

    parser.add_argument("--table_name", action="store", default="atcf", type=str,
                        help='table prefix, postfixed with point or line')
    parser.add_argument("--footprints", action="store_true",
                        help='compute footprints')
    parser.add_argument("--footprints_table_name", action="store", default="Track", type=str,
                        help='table name for footprints')
    parser.add_argument("--dbd", action="store", default=os.getenv('DATABASE_URL'), type=str,
                        help='database (postgresql://user:pass@host:5432/base_name)')
    parser.add_argument("-d", "--nb_days_to_update", action="store", type=int, default=10,
                        help="the number of days to be updated in the past")
    parser.add_argument("files", action="store", nargs='*', type=str,
                        help="atcf tracks stored in unique files : '*.dat' ; or in geopackages : '*.gpkg'")

    args = parser.parse_args()
    if sys.gettrace():
        logger.setLevel(logging.DEBUG)
        geopandas_postgis.logger.setLevel(logging.DEBUG)

    # Connect to an engine so it can send the data to the database
    engine = create_engine(args.dbd)
    con = engine.connect()

    # get sampling rate for time interpolation
    sampling_rate_in_minutes = cyclobs_db.get_config(con, key='track_sampling_rate')
    dtime = dt.timedelta(minutes=int(sampling_rate_in_minutes))

    # Read the Tracks and put them in a GeoDataFrame
    file_extension = ""
    for file in args.files:
        file_extension = os.path.splitext(file)[1]
        logger.info("Files extension is : {}".format(file_extension))
        break
    if file_extension == '.dat':
        gdf = read_dat(dtime, args.files, args.nb_days_to_update)
    elif file_extension == '.gpkg':
        print("Not implemented")
        sys.exit(1)

    if len(gdf.index) > 0:
        # Verifying if the GeoDataFrame correspond to the willing schema
        verify_schema(gdf)

        # No crs, so postgis will use geographic (not a projection)
        gdf.crs = None

        # Create then set the Geometry column
        gdf['geom'] = gpd.points_from_xy(gdf['lon'], gdf['lat'])

        final_gdf = gdf.set_geometry('geom')

        #final_gdf.rename(columns={"sid": "id"}, inplace=True)

        # Define index −> They will be Primary Keys in the database
        final_gdf.set_index(['sid', 'date'], inplace=True)

        # Push the GeoDataFrame to the database
        with con.begin():
            import_atcf(final_gdf, con, table_name=args.table_name, footprints_table_name=args.footprints_table_name,
                        if_exists='append')  # footprints=args.footprints,

        # Log the duration of the loading for this execution
        time_spent = time.time() - start_time
        if file_extension == '.dat':
            logger.info('inserted {len} atcf tracks in {time: .0}s ({len_by_time: .1}s / track)'.format(len=len(args.files),
                                                                                                        time=time_spent,
                                                                                                        len_by_time=time_spent / len(
                                                                                                            args.files)))
        elif file_extension == '.gpkg':
            logger.info('inserted one geopackage in %.0fs' % (len(args.files)))
