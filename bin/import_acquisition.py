#!/usr/bin/env python
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import geopandas as gpd
import geopandas_postgis
import argparse
import logging
import os

from cyclobs_db import SMAPInserter, SARInserter, SMOSInserter

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

geopandas_postgis.logger.setLevel(logging.DEBUG)

os.environ['HDF5_USE_FILE_LOCKING'] = 'FALSE'


def extends_dict(dict_to_extend, input_dict):
    for k, v in dict_to_extend.items():
        dict_to_extend[k].append(input_dict[k])


def build_dicts(files, get_attr_func):
    acq_dict = {
        "id": [],
        "mission": [],
        "mission_short": [],
        "instrument": [],
        "instrument_short": [],
        "startdate": [],
        "stopdate": [],
        "geom": [],
        "quicklook_path": []
    }
    product_dict = {
        "id": [],
        "filename": [],
        "grid_type": [],
        "id_Acquisition": []
    }

    for f in files:
        try:
            acq, product = get_attr_func(f)
            extends_dict(acq_dict, acq)
            extends_dict(product_dict, product)
        except Exception as e:
            print("Fail to process ", f)
            print(repr(e))

    return acq_dict, product_dict


def process_paths(paths, acq_type, product_type, clean):
    engine = create_engine(args.dbd)
    con = engine.connect()
    Session = sessionmaker(bind=engine)
    sess = Session()

    inserter = None
    if acq_type == "SMAP":
        inserter = SMAPInserter(product_type)
    elif acq_type == "SMOS":
        inserter = SMOSInserter(product_type)
    elif acq_type == "SAR":
        inserter = SARInserter(product_type)
        paths = paths[0]

    files = inserter.get_files(paths)
    acq_dict, product_dict = build_dicts(files, inserter.get_attrs_from_file)
    try:
        if clean:
            inserter.clean_database(sess)
        insert_acquisitions(sess.connection(), acq_dict, product_dict, "Acquisition", "Product")
        sess.commit()
    except:
        # on rollback, the same closure of state
        # as that of commit proceeds.
        sess.rollback()
        raise
    finally:
        sess.close()


def insert_acquisitions(con, acq_dict, product_dict, acq_table, product_table):
    logger.info("Starting to insert...")
    gdf = gpd.GeoDataFrame(acq_dict, geometry='geom')
    gdf.crs = None
    gdf.set_index('id', inplace=True)
    # Update existing rows and appending new ones in database
    gdf.to_postgis(acq_table, con, if_exists='append')
    logger.info("Successfully inserted {} rows into {}.".format(len(gdf.index), acq_table))

    df = gpd.GeoDataFrame(product_dict)
    df.set_index('id', inplace=True)
    # Update existing rows and appending new ones in database
    df.to_sql(product_table, con, if_exists='append')


if __name__ == "__main__":
    description = """import acquisition files reference and footprints into database"""
    parser = argparse.ArgumentParser(description=description)

    current_types = ["SAR", "SMOS", "SMAP"]

    parser.add_argument("--dbd", action="store", type=str, required=True,
                        help='database (postgresql://user:pass@host:5432/base_name)')
    parser.add_argument("-p", "--data-paths", action="store", type=str, nargs='+', required=True,
                        help='Data paths to process')
    parser.add_argument("--acq_type", action="store", type=str, required=True,
                        help='Type of acquisition to insert. Available types are ' + ", ".join(current_types))
    parser.add_argument("-t", "--product_type", action="store", type=str, required=True,
                        help="Product type of data (gridded or swath")
    parser.add_argument("--clean", action="store_true", default=False, help="Enable cleaning table before inserting")

    args = parser.parse_args()
    paths = args.data_paths
    dbd = args.dbd

    logger.info("DBD : {}".format(dbd))
    logger.info(f"Inserting {args.acq_type} with product_type {args.product_type}")

    process_paths(paths, args.acq_type, args.product_type, args.clean)


