# -*- coding: utf-8 -*-
# !/usr/bin/env python

import os, logging
from functools import wraps

import psycopg2
import pandas as pd
import geopandas_postgis
import glob
from netCDF4 import Dataset
import h5py
from shapely.wkt import loads
import datetime
from owi import L2CtoLight
from cyclobs_orm import *
import abc
from scipy.spatial import ConvexHull
from shapely.geometry import Polygon
import numpy as np
import time

pd.DataFrame.to_sql_legacy = pd.DataFrame.to_sql
pd.DataFrame.to_sql = geopandas_postgis.to_sql

logging.basicConfig()
logger = logging.getLogger(os.path.basename(__file__))
logger.setLevel(logging.INFO)

# Initialize memory monitor
mem_monitor = True
try:
    from psutil import Process
except ImportError:
    logger.warning("psutil module not found. Disabling memory monitor")
    mem_monitor = False


def timing(f):
    """provide a @timing decorator for functions, that log time spent in it"""

    @wraps(f)
    def wrapper(*args, **kwargs):
        mem_str = ''
        process = None

        if mem_monitor:
            process = Process(os.getpid())
            startrss = process.memory_info().rss

        starttime = time.time()
        result = f(*args, **kwargs)
        endtime = time.time()

        if mem_monitor:
            endrss = process.memory_info().rss
            mem_str = 'mem: %+.1fMb' % ((endrss - startrss) / (1024 ** 2))

        logger.debug('timing %s : %.1fs. %s' % (f.__name__, endtime - starttime, mem_str))

        return result

    return wrapper


def set_config(con, key, value):
    """
    set key,value in config table.
    value converted to string (user must use pickle if he wants to store complex objet)
    """
    # Append the pandas' DataFrame values to Constants table
    df_config = pd.DataFrame({'value': str(value)}, index=[key])
    df_config.index.name = 'key'
    df_config.to_sql('config', con, if_exists='append')


def get_config(con, key=None):
    """ get value for key from config table, or all keys (as a df) if key is None"""

    # Query a pandas representing a constant value
    sql = 'SELECT * FROM config'
    if key is not None:
        sql = sql + " AS c WHERE c.key = '%s'" % key
    df = pd.read_sql(sql=sql, con=con)

    df.set_index('key', drop=True, inplace=True)
    if key is not None:
        return df['value'][0]
    else:
        return df


class Inserter(abc.ABC):
    def __init__(self, instrument, instrument_short, mission, mission_short, product_type):
        self.instrument = instrument
        self.instrument_short = instrument_short
        self.mission = mission
        self.mission_short = mission_short
        self.product_type = product_type

    def get_files(self, paths):
        pass

    def get__attrs_from_file(self, file):
        pass

    def clean_database(self, sess):
        if self.mission is not None:
            logger.info(f"Delete Products for instrument {self.mission_short} and product {self.product_type}")
            # Removing previous Acquisitions to prevent non-existing files from being queried
            sess.query(Product).filter(and_(Product.id_Acquisition == Acquisition.id,
                                                Acquisition.mission_short == self.mission_short,
                                                Product.grid_type == self.product_type)) \
                .delete(synchronize_session=False)

            # Deleting childless parents
            sess.query(Acquisition).filter(Acquisition.products == None).delete(synchronize_session=False)

            logger.info("Finished deleting")

            # The next line is not needed as the relationship between acquisition and product is configured
            # on delete-orphan. https://docs.sqlalchemy.org/en/13/orm/cascades.html#cascade-delete-orphan
            # See configured relationship in cyclobs_orm
            # sess.query(Acquisition).filter(Acquisition.mission_short == self.mission_short).delete()
        else:
            logger.info(f"Delete Products for instrument {self.instrument} and product {self.product_type}")
            # Removing previous Acquisitions to prevent non-existing files from being queried
            q = sess.query(Product).filter(and_(Product.id_Acquisition == Acquisition.id,
                                                Acquisition.instrument == self.instrument,
                                                Product.grid_type == self.product_type)) \

            q.delete(synchronize_session=False)

            # Deleting childless parents
            sess.query(Acquisition).filter(Acquisition.products == None).delete(synchronize_session=False)
            logger.info("Finished deleting")

            # The next line is not needed as the relationship between acquisition and product is configured
            # on delete-orphan. https://docs.sqlalchemy.org/en/13/orm/cascades.html#cascade-delete-orphan
            # See configured relationship in cyclobs_orm
            # sess.query(Acquisition).filter(Acquisition.instrument == self.instrument).delete()


class LBandInserter(Inserter):
    def __init__(self, mission_name, mission_short, product_type):
        super().__init__("L-Band Radiometer", "L-Band Radiometer", mission_name, mission_short, product_type)

    def get_files(self, paths):
        files = []
        for path in paths:
            logger.info("Current base path: {}".format(path))
            if not path.endswith(".nc"):
                files = glob.glob("{}/**/*.nc".format(path), recursive=True)
            else:
                files = [path]

        return files

    def get_attrs_from_file(self, file):
        nc = Dataset(file)
        acq_id = os.path.basename(file)
        acq_dict = {
            "mission": self.mission,
            "mission_short": self.mission_short,
            "instrument": self.instrument,
            "instrument_short": self.instrument_short,
            "geom": loads(nc.footprint),
            "id": acq_id,
            "startdate": datetime.datetime.strptime(nc.measurementStartDate, "%Y-%m-%d %H:%M:%S"),
            "stopdate": datetime.datetime.strptime(nc.measurementStartDate, "%Y-%m-%d %H:%M:%S"),
            "quicklook_path": None
        }

        product_dict = {
            "id": os.path.basename(file),
            "filename": file,
            "grid_type": self.product_type,
            "id_Acquisition": acq_id
        }

        return acq_dict, product_dict


class SMAPInserter(LBandInserter):
    def __init__(self, product_type):
        super().__init__("Soil Moisture Active and Passive", "SMAP", product_type)


class SMOSInserter(LBandInserter):
    def __init__(self, product_type):
        super().__init__("Soil Moisture and Ocean Salinity", "SMOS", product_type)

    def get_files(self, paths):
        if "/home/datawork-cersat-public/cache/public/ftp/project/cyclobs/data/smos" in paths[0]:
            for path in paths:
                if not path.endswith(".mat"):
                    smos_files = glob.glob("{}/*_FIX.mat".format(path))
                else:
                    smos_files = [path]
            keep_files = []
            for file in smos_files:
                smosFileSplit = file.split("_")
                num = int(smosFileSplit[2][2:4])
                if "INVEST" not in file and num < 80:
                    keep_files.append(file)
            return keep_files
        else:
            return super().get_files(paths)

    # Retrieve the geom bounding box by only selecting the area containing valid data
    def get_footprint(self, smos_file):
        wind = smos_file['cart_grid']['rws10_mps']

        # Only selecting lon/lat coordinates for which there is valid data.
        lonValid = smos_file['cart_grid']["lon"][~np.isnan(wind)]
        latValid = smos_file['cart_grid']["lat"][~np.isnan(wind)]

        # Computing convexHull which will return a hull containing point indices
        # composing the bounding box around the valid data
        hull = ConvexHull(np.transpose([lonValid, latValid]))
        return Polygon(hull.points[hull.vertices])

    def get_attrs_from_file(self, file):
        if file.endswith(".mat"):
            acq_id = os.path.basename(file)
            with h5py.File(file, 'r') as f:
                # Extracting min and max date from timestamps specified for each pixel.
                # The dates are specified in number of days since 0 in UNIX time.
                # The min year in python is 1 so we have to do some manipulations.
                date_min = datetime.datetime(1, 1, 1) + datetime.timedelta(
                    days=float(np.nanmin(f['cart_grid']["swath_time"]))) - datetime.timedelta(
                    days=367)
                date_max = datetime.datetime(1, 1, 1) + datetime.timedelta(
                    days=float(np.nanmax(f['cart_grid']["swath_time"]))) - datetime.timedelta(
                    days=367)

                acq_dict = {
                    "mission": self.mission,
                    "mission_short": self.mission_short,
                    "instrument": self.instrument,
                    "instrument_short": self.instrument_short,
                    "geom": self.get_footprint(f),
                    "id": acq_id,
                    "startdate": date_min,
                    "stopdate": date_max,
                    "quicklook_path": None
                }

                product_dict = {
                    "id": os.path.basename(file),
                    "filename": file,
                    "grid_type": self.product_type,
                    "id_Acquisition": acq_id
                }

                return acq_dict, product_dict
        else:
            return super().get_attrs_from_file(file)


class SARInserter(Inserter):
    def __init__(self, product_type):
        super().__init__("C-Band Synthetic Aperture Radar", "C-Band SAR", None, None, product_type)
        self.translate_short = {"SENTINEL-1 A": "S1A", "SENTINEL-1 B": "S1B", "RADARSAT-2": "RS2",
                                "RADARSAT Constellation": "RCM"}

    def get_files(self, listing_path):
        reportSuffix = "sarwingL2X/ConcatprocessOk.txt"
        reportFile = os.path.join(listing_path, reportSuffix)
        logger.info("Reading {} and inserting netCDFs to DB".format(reportFile))
        if os.path.isfile(reportFile):
            # For each path, read .txt that reference ncfiles
            with open(reportFile) as ncReport:
                files = ncReport.readlines()
                return files
        else:
            logger.warning(
                "Could not find {}. Maybe L2C process wasn't triggered for this listing.".format(reportFile))

    def get_attrs_from_file(self, file):
        file = file.replace('\n', '')
        # FIXME the L2C file is opened here, so attributes used later are those from the L2C file,
        #  which can be confusing
        # Maybe, directly open a more official L2X file to use official attributes ?
        nc = Dataset(file)
        path_split = file.split("/")
        start_path_quicklook = "/".join(path_split[0:18])
        acq_id = os.path.basename(file)
        path_file = os.path.splitext(os.path.basename(file))[0]
        path_file_split = path_file.split("-")
        path_file_split[3] = "cm"
        path_file_split[6] = "3".zfill(len(path_file_split[6]))
        path_file_3km = "-".join(path_file_split)
        acq_dict = {
            "mission": nc.missionName,
            "mission_short": self.translate_short[nc.missionName],
            "instrument": self.instrument,
            "instrument_short": self.instrument_short,
            "geom": loads(nc.footprint),
            "id": acq_id,
            # FIXME attributes only set on L2C
            # FIXME use nc.measurementDate when using official formats
            "startdate": datetime.datetime.strptime(nc.firstMeasurementTime, "%Y-%m-%dT%H:%M:%SZ"),
            "stopdate": datetime.datetime.strptime(nc.lastMeasurementTime, "%Y-%m-%dT%H:%M:%SZ"),
            "quicklook_path": os.path.join(start_path_quicklook,
                                           "post_processing",
                                           "L2M",
                                           "post_processing",
                                           "L2MQL3",
                                           "{}-{}".format(path_file_3km,
                                                          "owiWindSpeed.png"))
        }

        product_dict = {}
        # Preparing fields for product table
        if "swath" in self.product_type:
            swath_path = L2CtoLight(file, 3, mode="sw")
            product_dict = {
                "id": os.path.basename(swath_path),
                "filename": swath_path,
                "grid_type": "swath",
                "id_Acquisition": acq_id
            }
        elif "gridded" in self.product_type:
            gridded_path = L2CtoLight(file, 3, mode="ll_gd")
            product_dict = {
                "id": os.path.basename(gridded_path),
                "filename": gridded_path,
                "grid_type": "gridded",
                "id_Acquisition": acq_id
            }

        return acq_dict, product_dict

    def clean_database(self, sess):
        # Removing previous SAR acquisitions in SARAcq table, Acquisition and Product tables.
        # This is to prevent duplicates when the commit id changes.
        sess.query(SARAcq).delete()

        super().clean_database(sess)
