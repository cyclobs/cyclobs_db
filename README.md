#  Cyclobs website's database handling.

If you are planning to install cyclobs as a whole, the documentation at https://gitlab.ifremer.fr/cyclobs/cyclobs is more complete and we recommend to follow it rather than this one.

## installation

installation is done with pip install, in a conda env


## Database initialization

> warning : you will need to have a *working* pgmodeler-cli >=0.9.2 in your path (not demo version)
> check it with ```pgmodeler-cli```

### Postgresql deployment

set DATABASE_URL (this is optionnal for upgrading):
```
export DATABASE_URL="postgres://<user_name>:<password>@<host>:<port>/<database_name>"
```

If host from DATABASE_URL is the install host, and the database is not allready set up, 
the installation process will not fail.

once installed, to install postgresql/postgis and create the database:

```
export DATABASE_URL="postgres://<user_name>:<password>@<host>:<port>/<database_name>"
sudo setup_postgis_root.sh
```

( this can be undone with `sudo pg_dropcluster --stop 9.5 cyclobs` )

and run the instalation again, ie:
```
pip install --upgrade git+https://gitlab.ifremer.fr/cyclobs/cyclobs_db -v
```

To create the database from the model, and fill it:
```cylobs_db_setup.sh --initdb```

if the database is allready inititalised, you can remove --initdb



### Filling the Database

it done during install. But it's can also be done later, or by cron.

```
cyclobsDBupdate.sh [--all-atcf]
```

by default, only lasts atcf files are updated. --all-atcf loads all atcf files


## Production database management

see ~sarwing/README.cyclobs








